# **Expert's Guide Overview**

This **Expert's Guide** intends to provide a comprehensive resource for contributing to the controlling of GEM DCS with regard to the users with expert privileges. The guide is handled by the same group that built the current DCS for GEM detectors.

The content of the guide offers step by step instructions on several important topics, from the basic settings applications needed to get set up to proceed with more advanced configuration mechanisms.

DCS system of the GEM GE1/1 station could be controlled by user with and without such expert privileges. Given the decreased limitations on performing actions related to the DCS, it is recommended for expert users, to refer both the **Shifter's Guide** and the **Expert's Guide**.