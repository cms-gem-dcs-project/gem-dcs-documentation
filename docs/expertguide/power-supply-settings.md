# **HV and LV Channel Power Supply Settings**

The **'Power Supply Settings'** panel is used to view and change limits of power supplies associated with HV and LV channels. This panel can be accessed by clicking the **"Chamber HV / LV Settings"** in the **'Settings'** panel.
	
![Placeholder](../assets/images/expertguide/Settings/power_supply.PNG){ loading=lazy : style="width:700px"}

??? info "Viewing Power Supply Values"
	* Select one Chamber from the table in the left hand side.
			
		* Background of the selected fields will change into blue.
		* The text on the selected Chamber field will change into 'Selected'.
		* The selected Chamber will change into blue in the disc representation.
				
	* Click on the **"Show Current Settings"** or **"Show Readback Settings"** button.
	* Information on selected Chamber will appear in the right hand side table.

??? info "Applying Changes to Power Supply Limits"
	* Select the Chamber(s) that requires a change in the power supply.
	* Move to the field where the change should occur.
	* Type the new value in the field.
	* Click on the **"Apply Limit Settings"** button to apply limits.
	* Confirm the action in the popup dialog box.

??? info "Associated Dialog Boxes"
	!!! tip " "
		![Placeholder](../assets/images/expertguide/Settings/apply_changes.PNG){ loading=lazy : style="width:401px" , align=left}
			
		This dialog box appears to get the final confirmation before applying changes.
			
	!!! tip " "
		![Placeholder](../assets/images/expertguide/Settings/settings_success.PNG){ loading=lazy : style="width:401px" , align=left}
			
		This dialog box appears if the requested changes were made successfully.

	!!! warning " "
		![Placeholder](../assets/images/expertguide/Settings/select_chamber.PNG){ loading=lazy : style="width:401px" , align=left}
			
		This dialog appears if the user has clicked on the **"Update Table"** button or the **"Apply Limit Settings"** button, without selecting a Chamber(s).
		
	!!! warning " "		
		![Placeholder](../assets/images/expertguide/Settings/select_field.PNG){ loading=lazy : style="width:401px" , align=left}
			
		This dialog appears if the user has clicked on **"Set Values to Selected Fields"** button, without selecting a field(s).
		
??? faq "Purpose of Buttons"
	- [x] **Show Current Settings** : Show current settings of a selected Chambers
	- [x] **Show Readback Settings** : Show readback settings of a selected Chambers
	- [x] **Clear Selection** : Remove all selected Super Chambers
	- [x] **Set Values to Selected Fields** : Set the value typed in the feild besides the button to selected HV property
	- [x] **Set i0 to All** : Set the provided i0 to all foils of a selected Chamber
	- [x] **Load Default** : Load pre-defined values for selected properties
	- [x] **Switch All On** : Switch ON all foils of a selected Chamber
	- [x] **Switch All Off** : Switch OFF all foils of a selected Chamber
	- [x] **Apply HV Settings** : Apply all changes with regard to all HV property value changes
	- [x] **Apply LV Settings** : Apply all changes with regard to all LV property value changes

			
	
	