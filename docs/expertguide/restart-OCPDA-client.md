# **Resolving HV and LV Communication Problems by Restarting the OCP DA Client**

If there is communication problem in mainframes (with regard to both LV and HV), the first step is to try restarting the OPC DA Client which usually resolves the issue. 

!!! tip "This can be done at any given time, even with the detectors ON, as it does not send any command. Instead it just reset the communication." 

!!! warning "It is recommended to avoid sending commands via DCS to hardware while resarting the OPC DA Client due to the possibility of losing the command."

1.	Visit the repository CMS Online via https://cmsonline.cern.ch/.

2.	Move to **'Common'** section **(I)**, select **'DCS'** option **(II)** and then select **'Pmon'** option **(III)**.
	
	![Placeholder](../assets/images/expertguide/Specifics/CMS_online_PMON.PNG){ loading=lazy : style="width:412px" }

3.	Select the project **'cms_gem_dcs_1'** **(IV)** from the list of **'Production Systems'** and move to **'PMON'** tab of the project destination.

4.	Select **"OPC DA Client"** radio button **(V)** from the **'Manager'** section.
	
	![Placeholder](../assets/images/expertguide/Specifics/OCP_DA_client.PNG){ loading=lazy : style="width:683px" }

5.	Press **"Stop"** button **(VI)** in the **'Project Status'** section.

6.	It will restart automatically therefore simply refresh the page.


