# **Utilizing the DCS Integration Test Panel**

Members of the cms-gem-dcs-experts e-group have access to the **'GEM DCS Integration Test'** panel via the terminal server. It allows to connect or disconnect GEM subsystems from automated actions through FSM and detector protection.

!!! danger "The panel is password protected and once opened, user will be logged in as a "GEMShifter". Leave this user logged in, don't change the user!."

!!! warning "Never change the user by employing the widget on the top right. Access the panel and use it ONLY with the "GEMShifter" user."

!!! info "Important Fundementals"
	**Always take or release ONLY the upper node with the name CMS, in order to avoid problems with the panel or the FSM.**

	* **TAKE** : Click on the locker and then select the **'Take'** option. 
	* **RELEASE** : Click on the locker and then select the **'Release All'** option (not 'Release').

## **Connect or Disconnect the System**

### **Connect the System**

1. Check if the system is not in local, on the part where subsystem are listed.
2. If they are not in local then take the CMS node.
3. On the right, click on the **"Start"** button on the right of the DIP status LED.

### **Disconnect the System**

1. On the right, click on the **"Stop"** button on the right of the DIP status LED.
2. On the CMS node, click on the locker and then click on **"Release All"** (if not visible, expand the list).

## **Check Detector Protection Status**

Verification of detector protection status is done employing the **'GEM Standby'** condition. 

??? danger "WAITING Status" 
	Detector protection is not fired.
	
??? danger "FIRED Status" 
	Detector protection is fired.

If the user clicked on **"Stop"** to stop the DIP subscriptions, the GEM standby condition will be **'frozen'** as it is. Although not recommended, in case it is fired, it is possible to reset to false by clicking on **"Deactivate"**. 

!!! warning "It is recommended to avoid disconnecting the system while the detector protection is fired."
