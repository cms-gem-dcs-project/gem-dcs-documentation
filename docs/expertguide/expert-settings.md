# **Expert Settings Overview**

![Placeholder](../assets/images/shifterguide/general-guides/settings.PNG){ loading=lazy : style="width:250px" , align=right}

The **"Settings"** button on the main interface provides the collection of settings available for changes. Most of the settings are specifically for expert access, whereas some do not require expert privileges.

!!! warning "If the requested changes expect expert privileges and if the user does not have expert privileges the it will be notified to the user." 
	
!!! tip "Refer the Shifter Guide for actions associated with settings that does not require expert privileges."

