# **What to do if a component is reinstalled?**

## **Reinstallation of a CAEN Component**

!!! info "In cases such as this the CMS_GEM_CAEN component erases the FSM structure previoulsy existed in the system."

!!! tip "If a component is reinstalled," 
	* Trigger the reinstallation of the CMS_GEM_Multichannel component (Refer the instructions in the section below)
	* Trigger the reinstallation of the CMS_GEM_GAS component (Refer the instructions in the section below)

## **Reinstallation of a Multichannel Component**

!!! danger "Most commonly identified issue with multichannel configurations is that, when installed, the configuration does not always import the alarm handling of the MC DPs in the project." 

!!! warning "Usually following the procedure is enough, but in case it does not work, it is recommended to contact the central DCS expert for assistance."

The user must trigger the reinstallation of the MC component by following the procedure below.

1.	Ask the DOC to take particular attention to GEMINI01 behavior and to notify of any issues that may arise.
2.	The user could try to induce an alarm by putting 0 uA current limit during the ramp and checking if the alarm is generated.
3.	If the alarm is not generated then try to reinstall the MC component by selecting force reinstall.

## **Reinstallation of a Gas Component**

In case of reinstallation of a gas component, it is imortant to check the alarm handling activation. 

### **Regarding All Cases of Reinstallation**

1.	Exclude from the gas FSM tree **'Distribution Stepper'** and **'Rack61 Stepper'** for the known Rack 61 state issue, if not excluded, the gas node will always be in a WARNING state.

2.	Check the status of the alarm handling by visiting the repository CMS Online via  https://cmsonline.cern.ch/. 

3.	Select **'DCS'** option **(II)** from the **'Common'** option **(I)**. 
	
	![Placeholder](../assets/images/expertguide/Specifics/reinstalled_cases.PNG){ loading=lazy : style="width:748px" }

4.	Click on the **'WinCC OA Access'** option **(III)**.

5.	Select **'cms_gem_dcs_1'** project **(IV)** from the list of **'Search dps...'**.
	
6.	Expand the project and click on the **DipSubscriptionsFloat** (once selected will be viible as in the image) to select a gas DP.
	
	!!! warning "It is normal that some DPs are active and some are not, if unsure, follow the instructions below to reactivate them. Not all the gas DPs have an active alert config, hence use CMSGEM_Di_Ch3.value as a reference." 

### **If Any Configuration is Reinstalled**

<span style="color:blue">**Not Default!**</span>

1.	Follow the instructions in the **"Alarm Set"** section to open the related panel. Refer the [“Gas Expert”](../expertguide/gas-expert.md) section for further details.
	
	??? info "Not Force Reinstalled Configurations (Default)"
		The values displayed in the panel will already be the correct ones, therefore suffice to set them all.
		
	??? info "Reinstalled Configurations (Not Default)"
		* Open the **'Save/Load Recipe'** panel. 
		* Load the recipe **'Gas_Standard'**. Refer the [“Recipe Settings”](../expertguide/recipe-settings.md) section. 
		* Set the alarm.
		
2.	Follow the instructions in the **"Mask/Unmask"** section to mask the following items. Refer the [“Gas Expert”](../expertguide/gas-expert.md) section for further details. 
	
	!!! info ""
		* `CMSGEM_Di_PT6104.value` 
		* `CMSGEM_Di_FE6102Ch1.value`, `CMSGEM_Di_FE6102Ch5.value`, `CMSGEM_Di_FE6102Ch6.value` 
		* `CMS_GEM_Di_Ch1.value`, `CMS_GEM_Di_Ch5.value`, `CMS_GEM_Di_Ch6.value` 

### **ONLY if the Configuration gem_operation_dp_V001 is Reinstalled**

<span style="color:blue">**Not Default!**</span>

1.	Should be fixed with the installation of gem_operation_dp_V003.

2.	If the gem_operation_dp_V001 configuration is reinstalled, it will cancel the DIP configuration of LHC, Magnet, DSS and GEMINI01 temperature DPs, therefore the user will have to reconfigure them manually.
	
	!!! info "It is not reinstalled by default when the GAS component is reinstalled."
	
	!!! warning "Do not force the reinstallation of this configuration unless 100% certain."

3.	The list of DPs is in the csv file 'DIPConfig_Monitoring.csv'.

4.	Go into one of the panels from LHC Magnet, DSS or GEMINI01 Temperature, press **"Dip Config"**, and then press **"Set Dip Config"**.

5.	Go to the **'Edit'** tab in the child panel. 

6.	Select **'Subscriptions'** and **'cms_gem_dcs_1:DIPConfig_Monitoring'** and wait for the system to finish loading.
	
	!!! warning "In any moment if the loading process is stuck, it is probable to have a currupted subscription. Ask for support in fixing it, because even the expert user cannot access it directly."

7.	In the **'New subscription'** area,
	
	!!! info "Fill in the information according to instructions below."
		* **Selected DIP Item** : Copy the elements one by one in the column 'DIP Publication' of the csv file.
		* **Tag** : Copy the corresponding element in the column 'Tag' of the csv file.
		* **DPE for Subscribed Data** : Copy the corresponding element in the column 'DPE' of the csv file 
		* Click on **'Add subscription'**.

	
