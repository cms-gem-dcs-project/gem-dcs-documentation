# **Changing Temperature and Frontend Limit Settings**

!!! warning "If the requirements are not clear or if not sure exactly what to do, do not request for changes. Changing values disregarding the guidelines will trigger alarms."

![Placeholder](../assets/images/expertguide/Settings/temp_frontend_limits.PNG){ loading=lazy : style="width:700px" }

??? info "View Temperature and FrontEnd Values"
	* Select one or more Super Chambers from the table in the left hand side.
		
		* Background of the selected fields will change into blue.
		* The text on the selected Super Chamber field will change into 'Selected'.
	
	* Click on the **"Update Table"** button.
	* Information on selected Super Chambers will appear in the right hand side table.

??? info "Applying Changes to the Limits"
	**If changing limits individually,**
	
	* Move to the field where the change should occur.
	* Type the new value in the field.
	* Click on the **"Apply Limit Settings"** button to apply limits.
	* Confirm the action in the popup dialog box.
	
	**If changing limits of multiple fields simultaneously,**
	
	* Select the row(s) and column(s) that requires changes by ticking on the box.
	* Type the value in the small field available next to the name.
	* Click on the **"Set Values to Selected Fields"** button to apply the typed value.
	* Click on the **"Apply Limit Settings"** button to apply limits.
	* Confirm the action in the popup dialog box.
	
??? info "Associated Dialog Boxes"
	!!! tip " "
		![Placeholder](../assets/images/expertguide/Settings/apply_changes.PNG){ loading=lazy : style="width:401px" , align=left}
		
		This dialog box appears to get the final confirmation before applying changes.
			
	!!! tip " "
		![Placeholder](../assets/images/expertguide/Settings/settings_success.PNG){ loading=lazy : style="width:401px" , align=left}
		
		This dialog box appears if the requested changes were made successfully.

	!!! warning " "
		![Placeholder](../assets/images/expertguide/Settings/select_chamber.PNG){ loading=lazy : style="width:401px" , align=left}
		
		This following dialog appears if the user clicked on the **"Update Table"** button or the **"Apply Limit Settings"** button without selecting a Chamber(s).
		
	!!! warning " "		
		![Placeholder](../assets/images/expertguide/Settings/select_field.PNG){ loading=lazy : style="width:401px" , align=left}

		This dialog appears if the user clicked on **"Set Values to Selected Fields"** button, without selecting a field(s).
		
	!!! warning " "
		This dialog appears if the value(s) the user has entered for warning limits are higher than error limits. Warning limits must always be less than error limits.
				
??? faq "Purpose of Buttons"
	- [x] **Update Table** : Update table with information on selected Super Chambers
	- [x] **Clear Selection** : Remove all selected Super Chambers
	- [x] **Set Values to Selected Fields** : Set the value typed in the feild besides the button to selected property
	- [x] **Apply Limit Settings** : Apply all changes wwith regard to all property value changes

