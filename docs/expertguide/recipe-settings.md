# **Changing Recipe Settings**

A set of standard values to be applied to the DPs in certain situations ae collectively referred to as a **recipe**. 

!!! info "Possible recipe types are HV settings, LV settings, Multichannel settings, and Gas Alarm settings."
	
The **Recipe Settings** panel can be accessed by, clicking on the **"Recipe Settings"** button in the **'Settings'** panel or by clicking on the **"Save / Load Recipe"** button in the main interface.

![Placeholder](../assets/images/expertguide/Settings/recipe_main.PNG){ loading=lazy : style="width:573px" }
	
### **Save Recipe Settings**

1.	In the **'Save Recipe'** section, provide the recipe kind and select the fact if it is on FSM or development mode.

2.	Select the correct recipe type. 
	
3.	If necessary edit the recipe type by means of the panel accessible via the **"Edit Recipe Type"** button.
	
	![Placeholder](../assets/images/expertguide/Settings/recipe_edit.PNG){ loading=lazy : style="width:524px" }

4.	Give the state and elements for the recipe.

5.	Click on the **"Save to ConfDB"** to save the recipe.

6.	If values require changes after saving, click on the **"Open HV/LV Settings"** button and do the necessary modifications in the **'HV/LV Settings'** panel.

### **Load Recipe Settings**

1.	Provide the recipe name and if necessary user can find the a list of recipes by clicking on the **"Open Recipe List"** button.
	
2.	Load information by clicking on the **"Load Recipe from DB"** and preview to ensure the accuracy.

	!!! warning " "
		![Placeholder](../assets/images/expertguide/Settings/specify_recipe.PNG){ loading=lazy : style="width:401px" , align=left}
	
		A recipe must be selected before sending a command to load the recipe from the database.
		
		If the **"Load Recipe from DB"** button was clicked without selecting a recipe, this dialog box will appear.

3.	Use the **'Recipe Limits'** panel to view and change limits. This panel can be opened by clicking on the **"Open Recipe Limits Panel"**.
	
	![Placeholder](../assets/images/expertguide/Settings/recipe_limit.PNG){ loading=lazy : style="width:874px"}

	??? info "Viewing Current Recipe Values"
		* Select one or more Super Chambers from the table in the left hand side.
			
			* Background of the selected fields will change into blue.
			* The text on the selected Super Chamber field will change into 'Selected'.
				
		* Click on the **"Update Table"** button.
		* Information on selected Super Chambers will appear in the right hand side table.

	??? info "Applying Changes to Recipe Limits"
		**If changing limits individually,**
	
		* Move to the field where the change should occur.
		* Type the new value in the field.
		* Click on the **"Apply Limit Settings"** button to apply limits.
		* Confirm the action in the popup dialog box.
	
		**If changing limits of multiple fields simultaneously,**
	
		* Select the column that requires changes by ticking on the box.
		* Type the value in the small field available next to the button.
		* Click on the **"Set Values to Selected Fields"** button to apply the typed value.
		* Click on the **"Apply Limit Settings"** button to apply limits.
		* Confirm the action in the popup dialog box.
	
	??? info "Associated Dialog Boxes"

		!!! tip " "
			![Placeholder](../assets/images/expertguide/Settings/apply_changes.PNG){ loading=lazy : style="width:401px" , align=left}
			
			This dialog box appears to get the final confirmation before applying changes.
			
		!!! tip " "
			![Placeholder](../assets/images/expertguide/Settings/settings_success.PNG){ loading=lazy : style="width:401px" , align=left}
			
			This dialog box appears if the requested changes were made successfully.

		!!! warning " "
			![Placeholder](../assets/images/expertguide/Settings/select_chamber.PNG){ loading=lazy : style="width:401px" , align=left}
			
			This dialog appears if the user has clicked on the **"Update Table"** button or the **"Apply Limit Settings"** button, without selecting a Chamber(s).
		
		!!! warning " "		
			![Placeholder](../assets/images/expertguide/Settings/select_field.PNG){ loading=lazy : style="width:401px" , align=left}
			
			This dialog appears if the user has clicked on **"Set Values to Selected Fields"** button, without selecting a field(s).
		
	??? faq "Purpose of Buttons"
		- [x] **Update Table** : Update table with information on selected Super Chambers
		- [x] **Clear Selection** : Remove all selected Super Chambers
		- [x] **Set Values to Selected Fields** : Set the value typed in the feild besides the button to selected property
		- [x] **Apply Limit Settings** : Apply all changes wwith regard to all property value changes

			
	
	