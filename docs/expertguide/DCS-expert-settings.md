# **DCS Expert Settings**

The **'DCS Expert Settings'** panel is used to view and change limits of time delays associated with detector protection. This panel can be accessed by clicking on the button with the same name in the **'Settings'** panel.
	
![Placeholder](../assets/images/expertguide/Settings/DCS_expert.PNG){ loading=lazy : style="width:621px" }

??? info "Associated Actions"
	* **HV Auto Restart Manager Settings**
		* Enabling or disabling auto restart
		* Setting the restart limit
		* Setting the HV DP lock time 
	* **DAQ LV Power Cycle**
		* Enabling or disabling of DAQ LV Power Cycle
	* **FPGA Temperature Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **GAS Ar% Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **GAS Distribution Rack Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **Magnet Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **HV Board Temperature Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **HV Channel Temperature Protection **
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **LV Easy Board Temperature Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	* **LV MAO Channel Temperature Protection**
		* Enabling or disabling protection
		* Setting the time delay before turning off
	
??? faq "Purpose of Buttons"
	- [x] **Disable** : Disable the relevant manager or protection property
	- [x] **Enable** : Enable the relevant manager or protection property
	- [x] **Set** : Set the typed value in the field to the relevant protection property
	- [x] **Reset Restart Count for All SChambers** : Reset restart count for all Super Chambers
	- [x] **DAQ LV PowerCycle (Disabled)** : Enable or disable the DAQ LV power cycle 
	
			