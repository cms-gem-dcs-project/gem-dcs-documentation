# **Alarm Masking and Unmasking Settings**

The alarm masking and unmasking panel for HV, LV and Gas is accessible via the **"LV/HV Alarm Mask/Unmask Settings"** option in **'Settings'** panel.

![Placeholder](../assets/images/shifterguide/general-guides/alarm_mask_unmask.PNG){ loading=lazy : style="width:600px" }

An expert can mask alarms to not reach the central DCS in case of irrelevant alarms caused by malfunctioning sensors, managable issues and similar cases that can be handled easily. 

By unmasking the alarms would return to their normal status of alerting all necessary parties in case of emergency alarms.

??? info "Alarms Available for Masking and Unmasking"
	* **LV Alarms** : LV Channel Alarms, LV Board Alarms
	* **HV Alarms** : HV Channel Alarms, HV Board Alarms 
	* **Gas Alarms** : Gas Mixer Alarms, Gas Rack Alarms, Gas Flow Cell Alarms
	* **Temperature Alarms** : DAQ Temperature, Cooling 

??? info "Actions Associated with Alarm Masking/Unmasking Panel"
	* It is possible to select single, multiple or all options in each tab and it is required to have selected at least one such option before commanding to activate or deactivate alarms.
	* If selecting more than 1 option user will have to keep the hold of already selected options via `'ctrl'` key.
	* It is possible to perform actions on alarm types indicated by the tick boxes by selecting one, multiple or all.

??? faq "Purpose of Buttons"
	- [x] **Deactivate Alarms for Selected DPEs** : Deactivate alarms for one or more selected alarms
	- [x] **Activate Alarms for Selected DPEs** : Activate alarms for one or more selected alarms
	- [x] **Deactivate Alarms for All DPEs** : Deactivate alarms for all alarms regardless a selection
	- [x] **Activate Alarms for Selected DPEs** : Activate alarms for all alarms regardless a selection

