# **Remote Connection to the Mainframe**

=== "From Inside CERN"

	??? info "Using the Web Browser" 
		* Open the web browser and type the 'Device Name' of the mainframe (given in registeration process) in the address bar.
		* Enter the user credentials.

	??? info "Using the Terminal" 
		* Type `ssh <userName>@<deviceName>` if the mainframe is CAEN SY4527.
		* Or type `telnet <deviceName > 1527` if the mainframe is CAEN SY1527.
			
=== "From Outside CERN"
	
	!!! warning "Web Browser cannot be used from outside CERN to connect to a mainframe remotel."
	
	??? info "Using the Terminal" 
		* Open a terminal window and log into personal lxplus account by typing `ssh -Y userName@lxplus6.cern.ch`.
		* Then log into cmsusr.cern.ch by typing `ssh -Y cmsusr.cern.ch`.
		* Enter the user credentials (not possible if the user does not have an account in cmsusr.cern.ch).
		* Log into the mainframe using `ssh admin@mainframeIPaddress` command. 
		* Provide the mainframe password.

The resultant display would be similar to the image below, where **'Ch#' column** presents the board number and the corresponding channel number in <BoardNumber>.<ChannelNumber> format.

![Placeholder](../assets/images/devguide/Special/mainframe_remote.png){ loading=lazy : style="width:679px" } 

