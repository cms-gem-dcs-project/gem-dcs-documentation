# **Changing CAEN Settings**

The **'CAEN Status Settings'** panel allows the expert users to perform changes on CAEN modules associated with the GEM detectors via the DCS. 

## **CAEN Module Settings**

The panel presents the connections in the CAEN module status monitor down to a detailed level for both HV and LV systems from mainframes to HV and LV boards.

![Placeholder](../assets/images/expertguide/Settings/CAEN_settings.PNG){ loading=lazy : style="width:999px" }

!!! danger "For further understanding of the CAEN module connection, refer ["GEM Structural Delineation"](../devguide/GEM-structure.md) in the Developer's Guide."

Clicking on the boxes representing CAEN modules provide information at a given moment, associated with the module.

??? info "Single Click"
	* A single click provides a popup panel summerizing the CAEN status of the selected module.
	* Following image is of a summary popup panel for a MAO power module obtained by a single click.
	
		![Placeholder](../assets/images/expertguide/Settings/MAO_single_click.PNG){ loading=lazy : style="width:433px" }

??? info "Double Click"
	* A single click provides a popup panel of standard settings of the selected module.
	* Following image is of a standard settings panel for a MAO power module obtained by double clicking.
	
		![Placeholder](../assets/images/expertguide/Settings/MAO_double_click.PNG){ loading=lazy : style="width:404px" }

## **Communication Settings**

It is possible to change communication OPC UA settings using this panel. A single click on the **"Communication Settings"** buttons will lead to the following panel.

![Placeholder](../assets/images/expertguide/Settings/CAEN_communication.PNG){ loading=lazy : style="width:573px" }

Left and right clicking on the **'Currently Installed OPC UA Servers'** allows the user to select and open the configurator panel for the selected server respectively.

Actions can be defined in the **'Define Actions'** section, allowing the user to perform such defined actions on selected servers.

