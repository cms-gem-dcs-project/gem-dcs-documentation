# **DCS Panel Overview**
 
![Placeholder](../assets/images/shifterguide/general-guides/dcs_interface.PNG){: style="width:995px"}

!!! info "A single click on the relevant tab, button or icon will present a new interface, a new menu of options, a pop-up panel or an information panel." 

!!! warning "**Users must have privileges assigned to them in order to perform certain tasks.** If not the system will automatically restrict the options that are above the limit of privileges for the user that logged into the system."


