# **Detector Safety System (DSS)**

The DSS reacts if there is an emergency such as fire, smoke, water leak etc. The typical reaction of the DSS is to cut the power of the rack involved in the emergency, which is basically lead by PLC sensors mounted on each rack.

The **'DSS Info'** tab in the main interface of DCS provides an overview for the whole system i.e. HV and LV rack monitoring etc. The DSS is controlled and managed by a central dedicated team.

## **GEM DSS Actions**

!!! warning "Definitions for the reactions to events is still ongoing, therefore it is recommended to contact the [expert](../contact.md) if any of the DSS alarm notifications were recieved.!"

Following are a few examples for DSS notifications, explanation for the recieved notification and the course of actions to be taken after identifying the problem that triggered the notification.

??? example "Example 1 : DSS SMS 1"
	??? failure "You recieved this message :"
		`Alarm : AL_Syno_SCX5_Emergency_Stop_YE_Minus_1`
		
	??? info "What does it mean ?"
		The synoptique button regarding YE-1 in control room has been pushed.
		
	??? bug "What is the action to try out?"
		* O_Cut_Rack_Power_UXC55_YE1_Minus_Near_X2V33 (delay 100)

??? example "Example 2 : DSS SMS 2"
	??? failure "You recieved this message :"
		`Alarm : AL_HV_Switch_Off_On_YE1_Minus`
		
	??? info "What does it mean ?"
		A problem has triggered the cut off of all the HV racks of the YE-1.
		
	??? bug "What is the action to try out?"
		* O_Rack_Cut_Power_USC55_S1H13
		* O_Rack_Cut_Power_USC55_S1H12

??? example "Example 3 : DSS SMS 3"
	??? failure "You recieved this message :"
		`DSS CMSX Alarm : AL_UPS_LOW_VOLTAGE_BATTERY_ON`
		
	??? info "What does it mean ?"
		The low voltage mainframe has been powered from battery for more than 10 seconds.
		
	??? bug "What is the action to try out?"
		* O_Cut_Rack_Power_UXC55_YE1_Minus_Near_X2V33
		* O_Cut_Rack_Power_USC55_S4F03
		* (O_Cut_Rack_Power_USC55_S4F02)
	
	