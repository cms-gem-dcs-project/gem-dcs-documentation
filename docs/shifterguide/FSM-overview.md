# **Finite State Machine (FSM)**

## **FSM Representation in DCS**

FSM provides an independent abstract representation of an experiment at a given time. GEM DCS is controlled by a higher level node allocated for the CMS detector which is controlled by the top node assigned for LHC.  

**FSM in the GEM DCS allows the control of the whole system via a single node by calculating the state of the system and propagating actions through a node hierarchy.** 

The section presenting the FSM in the DCS main interface provides the user with information on,

1.	FSM Hierarchy 

2.	FSM Control and Majority

## **FSM Hierarchy**

The top part of the section provides the means to take, disable or release control of the FSM and allows backward or forward action propagation via the hierarchy.

![Placeholder](../assets/images/shifterguide/FSM/fsm_hierarchy.PNG){ loading=lazy : style="width:376px"} 

The CMS_GEM System is directly connected to its child nodes that control logical and hardware parts of the GEM DCS by means of the FSM.

??? info "Accessible Information"
	* Systems, subsystems and their states
	* Availability of systems and subsystems
	
??? info "Possible Actions"
	* Taking local control of the FSM from the central FSM
	* Releasing local control of the FSM to the central FSM
	* Enabling and disabling systems or subsystems
	
### **Logical System Hierarchy**

The hierarchy of the logical system could be accessed via the **‘GEM_Logical’**  subsystem of **‘CMS_GEM’** system. Refer to the below representation of the logical hierarchy structure.

![Placeholder](../assets/images/shifterguide/FSM/logical_system_hierarchy.PNG){ loading=lazy : style="width:806px"} 

Each subsystem throughout the hierarchy may or may not have child nodes connected to them that facilitate action propagation and control. 

### **Hardware System Hierarchy**
 
The hierarchy of the hardware system could be accessed via the **‘GEM_Hardware’**  subsystem of **‘CMS_GEM’** system. Refer to the below representation of the logical hierarchy structure.

![Placeholder](../assets/images/shifterguide/FSM/hardware_system_hierarchy.PNG){ loading=lazy : style="width:909px"} 

Each subsystem throughout the hierarchy may or may not have child nodes connected to them that facilitate action propagation and control. 

## **FSM Control and Majority**

The bottom part of the section presents the control information by indicating the current owner of the FSM at any given time and buttons for easy controlling of the FSM. Majority information are presented in a tabular format.

![Placeholder](../assets/images/shifterguide/FSM/fsm_control.PNG){ loading=lazy : style="width:357px"} 

??? info "Accessible Information"
	1. Control level of the FSM (local or central) at a given time.
	2. Owner of the FSM at a given time.
	3. Majority information on Super Chamber, HV and LV status in percentages and ratios.
		* Percentage (Default) : Shows the percentages of devices in the given state
		* Ratio : Shows the numbers in the format '(Number of Devices in the State) / (Total Number of Devices)
		
??? info "Majority Color Code"

??? faq "Purpose of Buttons"
	- [x] **FSM Control** : Control level (local or central) and the owner of the FSM at the moment of access
	- [x] **Release Control** : Relase FSM from local to central
	- [x] **Take Local Control** : Take local control from the central control

--8<-- "docs/includes/abbreviations.md"
