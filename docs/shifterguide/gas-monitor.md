# **Gas Status Monitor** 

![Placeholder](../assets/images/shifterguide/status-monitors/gas_status.PNG){ loading=lazy : style="width:218px" , align=right }   

This section associates with the **‘Gas’** interface. 

The cells indicating **'Mixer'**, **‘Rack 1’** and **‘Rack 2’** provides informtion on respective component if clicked on the cell.

## **Mixer Status**

!!! info "" 
	A single click on the **"Mixer"** results on the same panel that appears for clicking on **"Details"** button in the **‘Services’** Mode of the Gas interface. 

## **Rack Status**

!!! info ""
	![Placeholder](../assets/images/shifterguide/status-monitors/gas_status_monitor.PNG){ loading=lazy : style="width:250px" , align=left } 

	Clicking on either **"Rack 1"** or **"Rack 2"** will result in a panel displaying the Rack name, on/off status, pressure values and statuses. 

	A single click on the **"Show Trends"** button will result in a trend panel allowing the user to view trends for the selected Rack on the basis of pressure.

--8<-- "docs/includes/abbreviations.md"
