# **Common Errors and Actions**

## **Common Multichannel Errors**

!!! info "HV / LV Board State"
	For HV and LV boards the status of each channel is represented by a number below is the list of possible status of multichannel boards. If more than of these status are true, the status is their sum which is evident in cases of common errors.

	![Placeholder](../assets/images/shifterguide/general-guides/HV_LV_errors.PNG){: style="width:232px"}

??? failure "Trip Alerts"
	* A channel trips when it draws a current Imon greater than the current limit I0 for a timelapse greater than tripTime. 
	
	* It will be automatically switched off and, if the board is in GEM mode, all the channels of the same layer will also siwtched off (the channels of the other layer won't be affected).
	
	* <span style="color:red">The trip is usually preceded by an overcurrent (OvC) therefore it is possible to recieve just the OvC alert.</span>

??? failure "Overcurrent Alert"
	* This error happens when the current Imon exceeds the current limit I0 for a short time (less than tripTime). 
	* Does not switch off automatically the channel.
	* If the current trend in the past hours/days is anomalous, call an expert to evaluate the health of the channel. Otherwise, leave the channel on.

??? failure "Unplugged Alert"
	* The unplugged errors happens when the channel microcontroller is unable to communicate with the motherboard microcontroller. 

Most of above mentioned errors could be handled by appropriate user actions after identifying the problem, acknoledging the alarm and solving the issue. 

!!! warning "If unsure or unable to understand the alarm or the reason for the notification, it is recommended to contact an [expert](../contact.md)."	

Following are the actions that can be taken in order to resolve the common error states mentioned above.

??? bug "Trip / Overcurrent : Expected Actions for Error Notifications"
	* Open the dcs and open the Monitor panel where on the top-right side of the page to view the affected channel(s) and its status.
	* Open the trendpage relative to the faulty channel(s) and check if there has been some suspicious trend in the past hours/days.
	* If the past behaviour look ok, try to switch on the chamber.
	* Click on the **"Clear HV Alarm"** button. If you don't, the channels won't be swtich on and change the I0 to 50 before sending the command to switch on.
	* When the channels are ramping up they can draw a current much higher than 2: if the current limit is not changes, tripping of a channel is possible immediately as soon as switched it on.
	* Switch the layer on, in the relevant **'Change Settings'** page click on **"Switch ALL On"**, then click **"Apply"** and confirm with **"OK"**.
	* Observe the actual voltage increases up to the V0 on the right-most part of the **'Change Settings'** panel. (Monitor the status of each channel during the ramping up via **'Status Monitor' panel"
	* Switch on all chnnels.
	* Change the I0 to 2 (cosmics) or 10 (stable beams) again and use **"Set I0 to ALL"** button, then click on **"Apply"** and confirm.
	* Acknowledge the alert in the alarm screen. 

??? bug "Unplugged : Expected Actions for Error Notifications"
	* Click on the **"Clear HV Alarm"** button and then power cycle the channel.
	* If it does not work, call an expert.

## **Common Gas Errors**

!!! info "Mixer / Rack / Gas System State"
	The different elements which compose the gas system have a DP referred to as Step, which presents the state of operation the particular element is in. 
	
	Step is usually an integer, with many possible values but the DCS has declared to categorize such values into 3 ranges. 	
	![Placeholder](../assets/images/shifterguide/general-guides/gas_errors.PNG){: style="width:465px"}

The errors related to the gas system are mainly divided into 3 categories.

??? failure "1 : Mixure Related Errors"
	<span style="color:red">**Warning / Error in the Mixture**</span>
	
	`Alert: too high percentage in mixture cms_gem_dcs_1:CMSGEM_Mx_L1CompRatioAS.Value= 69.337471008301 (multi range alert triggered); Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16`

	<span style="color:red">**Warning / Error on Gas Pressure**</span> 
	
	`Alert: too low pressure cms_gem_dcs_1:CMSGEM_Mx_PT1009.Value= 0.0039072041399777 (multi range alert triggered); Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16`

??? failure "2 : Rack Related Errors"
	<span style="color:red">**Warning / Error in Racks**</span>
	
	`Warning: low pressure cms_gem_dcs_1:CMSGEM_Di_PT6125.Value= 0.35897445678711 (multi range alert triggered); Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16`

??? failure "3 : Flow Cell Related Errors"
	<span style="color:red">**Warning / Error on the Input Flow**</span>
	
	`Alert: check the input flow cms_gem_dcs_1:CMSGEM_Di_FE6102Ch4.Value= 0.199999999999 (multi range alert triggered); Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16`
		
	<span style="color:red">**Warning / Error on Difference Input / Output Flow**</span>
	
	`Alert: check the input and output flow cms_gem_dcs_1:CMS_GEM_Di_Ch4.Value= 1.6999998092651 (multi range alert triggered); Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16`

From the hardware point of view the gas system is completely handled by the CERN Gas Group and actions to be taken by the user are identifying the problem and notifying to the gas expert.

??? bug "Expected Actions for Error Notifications"
	* Open the DCS and identify the channel involved in the error
	* Understand if it is a warning (yellow) or an alert (red)
	* Wait a couple of minutes to see if it is only a fluctuation or a real problem
	* Follow the instructions in the table
	
	![Placeholder](../assets/images/shifterguide/general-guides/gas_action.PNG){: style="width:641px"}

!!! warning "In case the problem is not solved, the chambers can survive without the flow for at least 15 conservative minutes. After that, they should be switched off."

--8<-- "docs/includes/abbreviations.md"
