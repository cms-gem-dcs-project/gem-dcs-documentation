# **Detector Protection**

The mid section has modified detector protection status representations. Note the following array of protector options boxed in red.

![Placeholder](../assets/images/shifterguide/general-guides/detector_protection_options.PNG){ loading=lazy : style="width:710px" }

!!! warning "Only experts have access to change the detector protection settings, others can only view the status of each protection type by clicking on the relevant button in the main interface."

## **LHC Handshake**

A single click on the **"LHC Handshake"** button will result in a list of conditions associated with GEM detector and LHC via direct communication. 

The LHC signal could be on injecting, dumping or adjusting beam conditions and the DCS must be connected through the central DCS for proper functionality of such signals.

!!! example ""
	![Placeholder](../assets/images/shifterguide/general-guides/LHChandshake.PNG){ loading=lazy : style="width:404px" , align=left }
	
	Content has not been updated yet.

!!! info "The communication line in "LHC Handshake" from LHC to the GEM detector has the expectation of a return response with regard to all messages." 

## **LHC Condition**

A single click on **"LHC Condition"** button will result in the following list of conditions associated with the GEM detector and LHC in case of emergencies associated with no expectation of return response.

!!! example ""
	![Placeholder](../assets/images/shifterguide/general-guides/LHCcondition.PNG){ loading=lazy : style="width:408px" , align=left }
	
	Content has not been updated yet.
	
## **Gas Protection** 

A single click on the **"Gas Protection"** button will provide the information on gas protection information and triggers.

!!! example ""
	![Placeholder](../assets/images/shifterguide/general-guides/gas_protection.PNG){ loading=lazy : style="width:270px" , align=left }
	
	Content has not been updated yet.

## **Temperature Protection** 

A single click on the **"Temperature Protection"** button will provide the information on temperature protection information and triggers of modules employed in the system. 

!!! example ""
	![Placeholder](../assets/images/shifterguide/general-guides/temperature_protection.PNG){ loading=lazy : style="width:270px" , align=left }
	
	Content has not been updated yet.

## **Magnet Protection** 

A single click on the **"Magnet Protection"** button will provide status information on magnet protection information and triggers in the system.

!!! example ""
	![Placeholder](../assets/images/shifterguide/general-guides/magnet_protection.PNG){ loading=lazy : style="width:300px" , align=left }
	
	Content has not been updated yet.

--8<-- "docs/includes/abbreviations.md"
