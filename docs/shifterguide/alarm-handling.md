# **Alarm Notifications and Handling**

## **Alarm Screen**

It is possible to open the **'Alarm Screen'** of the DCS from different destinations in the system. It contains all the alarms related to all the different parts of the system.

![Placeholder](../assets/images/shifterguide/general-guides/alarm_screen.png){ loading=lazy : style="width:767px" } 

??? info "Color Code"
	![Placeholder](../assets/images/shifterguide/general-guides/alarm_color_code.PNG){ loading=lazy : style="width:337px" } 

??? info "Accessible Information"
	* The level of the alarm (W : Warning, E : Error, F : Fatal).
	* DPE involved with the alarm.
	* A description of the alarm.
	* A text associated to the alarm.
	* The direction of the alarm as CAME or WENT.
	* The Value of the DPE in the alarm.
	* The acknowledgment status for the alarm.
	* The time stamp

## **Acknowledging Alarms**

It is important to acknowledge the alarms by clicking on the **"Acknowledge"** button, such that further notifications will be sent in the case of continuation of problematic states.

!!! danger "The GEM DCS project is called cms_gem_dcs_1 and all the alarms coming from other systems can be ignored."

!!! warning "The CMS Email or SMS notifications doesn’t send 2 times, if the first notification related to the variable alert was not acknowledged."

## **Alarm Notification System**

When an alarm is raised, following actions are taken automatically by the DCS.

* An **SMS** is sent to the **GEM DOC's phone** (+ a list of few experts).
* An **E-mail** is sent to the list **cms-gem-dcs-notifications**.
* The alarm appears in the **Alarm Screen**.
* The background color of the indicator relative to the variable in error changes color.



