# **What is DCS?**

Detector is too complex and it is not possible to operate a detector by a small crew of experts. Detector has to run without interruption for many years, except in long shutdown periods. In order to make it possible to operate the detector easily, the name **"Detector Control System" (DCS)** comes into the picture. 

![Placeholder](../assets/images/shifterguide/DCS_image.png){: style="width:300px" , align=right}

!!! tip "DCS Purpose"
    * Continuous and safe operation of the detectors
    * High Voltage (HV) and Low Voltage (LV) controlling 
	* High Voltage (HV) and Low Voltage (LV) monitoring
	* Monitoring of gas system and environmental parameters
	* External system communication

DCS system of the GEM GE1/1 station has been developed based on a **"Supervisory Control and Data Acquisition" (SCADA)** software referred to as **"WinCC OA"**.

!!! info "5 Main Sections of the GEM DCS"
    * Standard HV or LV control and monitoring 
    * Finite State Machine (FSM)
	* Gas and environmental parameters monitoring
	* Radiation monitoring (RadMon)
	* LHC, magnet, DSS, temperature monitoring
	




