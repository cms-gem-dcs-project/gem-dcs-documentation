# **FSM Rules**

## **High Voltage Nodes**

### **HV Chamber Layer / GEMINI_HV Node**

??? info "HV Chamber Children (GEMINI_Layer)"
	* The **standard HV** chamber has a single child of type **HVChannel**
	* The **multichannel HV** chamber has 7 children of type **HVChannel**

??? info "GEMINI_HV Children"
	* The **GEMINI_HV** has 2 children of type **HVChamber**
	
### **GEM_HV Node**

??? info "GEM_HV Children"
	* The **GEM_HV** has 5 children (for the Slice Test) of type **GEMINI_HV**

### **Low Voltage Nodes**

### **LV Chamber Layer / GEMINI_LV Node**

??? info "LV Chamber Children (GEMINI_Layer)"
	* The **LV_Chamber** has 3 children of type **LVChannel**

??? info "GEMINI_LV Children"
	* The **GEMINI_LV** has 2 children of type **LVChamber**
	
### **GEM_LV Node**

??? info "GEM_LV)"
	* The **GEM_LV** has 5 children (for the Slice Test) of type **GEMINI_LV**

### **Gas System**

### **Top Level Node (GEM_ENDCAP_Minus)**

??? info "GEM_ENDCAP_Minus Children"
	* The **GEM_ENDCAP_Minus** chamber has 3 children of type **GEM_HV**, type **GEM_LV** and type **Gas_System**


