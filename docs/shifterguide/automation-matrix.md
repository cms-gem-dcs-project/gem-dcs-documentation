# **Automation Matrix**

The **Automation Matrix** collects all the actions (and following states) which are sent to (and reached by) each subdetector in each beam mode change or at a handshake warning. 

Use the link https://cmsonline.cern.ch/webcenter/portal/cmsonline/pages_common/dcs/automation to access the **Automation Matrix**. There are 2 main matrices to consider in the context.

## **<span style="color:DodgerBlue">Beam Machine Mode</span> Matrix**

!!! info "GEM follows the <span style="color:DodgerBlue">**Beam Mode**</span>." 
		
![Placeholder](../assets/images/shifterguide/general-guides/beam_machine_mode.PNG){ loading=lazy : style="width:721px" } 

## **<span style="color:Teal">Handshake</span> Matrix**

The <span style="color:Teal">**Handshake**</span> is a signal coming from the LHC in particular beam conditions, like injection, dump or when they move back from 'Stable' beams to 'Adjust'.

!!! tip "GEM does not respond to the <span style="color:Teal">**Handshake**</span>, therefore does not have to wait for before continuing the handshake procedure."

![Placeholder](../assets/images/shifterguide/general-guides/cms_online_handshake.PNG){ loading=lazy : style="width:719px" } 


## **When does the Automation Matrix work?**

The **Automation Matrix** works ONLY if the GEM is included in Central DCS, which is the reason, as general rule, GEM must always be included in Central, except for moments of intervention requested by experts.

## **What are the duties associated with the Automation Matrix?**

One of the duties assigned for the central DCS Shifter is to check that all the sub-systems follow the **Automation Matrix** properly. If not it is expected of the Shifter to contact the GEM DOC.

