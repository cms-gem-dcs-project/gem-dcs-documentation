# **What's in the Shifter's Guide ?**

The **Shifter's Guide** intends to provide an integrated and complete documentation source for an operator.

The main interface of the GEM DCS has several associated sections, tabs and buttons, each of which allows effortless visualization and controlling of the system. The main DCS interface system is divided into 2 regions. 

* **Common Region** : Common to all views (including all options of "Tab Region") and is always visible. 
* **Tab Region** : Tabs can be selected according to user requirements. 

!!! info "Shifter Guide Content"
	![Placeholder](../assets/images/shifterguide/general-guides/dcs_interface.PNG){: style="width:995px"}

Upon starting the DCS, all sections are disabled and user is required to login before operating the system. 

!!! warning "Depending on the level of privileges users might have restrictions on applying changes to the system. For GEM_Expert all of fuctionality is activated and for GEM_User limited fuctionality is given." 

--8<-- "docs/includes/abbreviations.md"
