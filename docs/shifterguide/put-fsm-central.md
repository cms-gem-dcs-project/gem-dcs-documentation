# **Inclusion or Exclusion of GEM FSM in Central DCS**

!!! info "User must be the owner of the FSM to do the following steps. If not, ask from the current owner to release FSM in central or take FSM back local for you."

!!! tip "<span style="color:red">For any reason you should not keep the FSM without an owner.</span> Communicate with the technical shifter in CMS control room (LHC-Point5), when releasing the FSM in central or taking it back to local."
	
## **Inclusion of GEM FSM in CMS (Release FSM in Central)**

### **Standard Method to Release FSM in Central**

1. 	First inform the technical shifter of the intention to release the FSM in central. 

2.	Click on the button **"Release Control"** and confirm the release from local.
	
	![Placeholder](../assets/images/shifterguide/FSM/release_button.PNG){ loading=lazy : style="width:385px" }
	
3.	The technical shifter can see (in the central DCS screen) about the released FSM and he/she will include the GEM FSM in CMS (central).
	
	!!! info "A gray color crossed and opened padlock in **“GEM_Logical”** node will be visible if the node is properly  in the **“Exclude&LockOut”** state."	
	
	![Placeholder](../assets/images/shifterguide/FSM/in_central.PNG){ loading=lazy : style="width:402px" }
	
4. 	The FSM is in Central and the long tab will indicate the current owner, in this case, the technical shifter !

### **Alternate Method to Release FSM in Central**

1. 	First inform the technical shifter of the intention to release the FSM in central. 

2. 	Click on the padlock in **“GEM_Logical”** node **(A)**.

	![Placeholder](../assets/images/shifterguide/FSM/exclude_lock_out.PNG){ loading=lazy : style="width:401px" }
	
3. 	Click on the **“Exclude&LockOut”** button **(B)**.

4. 	The technical shifter can see (in the central DCS screen) about the released FSM and he/she will include the GEM FSM in CMS (central).
	
	!!! info "A gray color crossed and opened padlock in **“GEM_Logical”** node will be visible if the node is properly  in the **“Exclude&LockOut”** state."	
		
	![Placeholder](../assets/images/shifterguide/FSM/in_central.PNG){ loading=lazy : style="width:402px" }
	
5. 	Go inside the **“GEM_Logical”** node, observe the red colored locked padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes which means that someone else owns these FSM nodes.
	
	![Placeholder](../assets/images/shifterguide/FSM/node_central.PNG){ loading=lazy : style="width:398px" }

6. 	Now the FSM is in Central and the owner at the moment in this case is the technical shifter !
	
## **Exclusion of GEM FSM from CMS (Take FSM in Local)**

### **Standard Method to Take FSM in Local**

1. 	Inform the technical shifter of the intention to take the FSM in local. 

2.	Click on the button **"Take Control"** and confirm the release from local.
	
	![Placeholder](../assets/images/shifterguide/FSM/take_button.PNG){ loading=lazy : style="width:384px" }
	
3.	The padlock will become locked and color will be changed into green. At the same time, the technical shifter can see that you have taken the FSM.
		
5. Now the FSM is in Local and the green tab above the buttons will indicate you as the owner !

### **Alternate Method to Take FSM in Local**

1. 	Inform the technical shifter of the intention to take the FSM in local and wait until technical shifter releases the GEM FSM from CMS. 

2. 	If checked inside the **“GEM_Logical”** node, the gray color opened padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes will be visible.
	
	![Placeholder](../assets/images/shifterguide/FSM/node_released.PNG){ loading=lazy : style="width:391px" }
	
3. 	Click on the padlock in **“GEM_Logical”** node **(A)**.
	
	![Placeholder](../assets/images/shifterguide/FSM/unlock_out_include.PNG){ loading=lazy : style="width:426px" }
	
4. 	Click on the **“UnlockOut&Include”** button **(B)** and then the padlock will become locked and change into green color. At the same time, the technical shifter can see that you have taken the FSM.
		
5. Now the FSM is in Local !

## **Possible Error Messages**

!!! failure "If you receive following message when you click **“UnlockOut&Include”**, this means still the FSM is in central and technical shifter did not release the FSM yet."
	![Placeholder](../assets/images/shifterguide/FSM/fsm_error.PNG){ loading=lazy : style="width:461px" }
	
	**If this happens,**
	
	- Click **“Dismiss”** to close the message.
	- Wait until technical shifter release the FSM or ask again to release the FSM. 
	- Then click on the padlock in **“GEM_Logical”** node and click on the **“Exclude&LockOut”** button. 
	- Click again on the padlock in **“GEM_Logical”** node and click on the **“UnlockOut&Include”** button.

--8<-- "docs/includes/abbreviations.md"