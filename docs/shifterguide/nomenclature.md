# **Nomenclature** 

!!! danger "Please note that this section contains not all but a simple set of nomenclature strictly necessary for better understanding of the system and in the process of referring the documentation website for DCS operations."

## **CERN Accelerator Complex**

![Placeholder](../assets/images/shifterguide/general-guides/CERN_complex.PNG){ loading=lazy : style="width:519px"} 

## **CMS Detector Layout**

![Placeholder](../assets/images/shifterguide/general-guides/CMS_Muon.PNG){ loading=lazy : style="width:851px"} 

## **GEM Detector Installation**

![Placeholder](../assets/images/shifterguide/general-guides/GEM_GE11.PNG){ loading=lazy : style="width:631px"} 

## **GEM Detector Position**

![Placeholder](../assets/images/shifterguide/general-guides/GEM_positions.PNG){ loading=lazy : style="width:613px" } 

## **GEM Detector Chamber Structure**

![Placeholder](../assets/images/shifterguide/general-guides/Triple_GEM.PNG){ loading=lazy : style="width:930px"} 

!!! info "GEMINI = A pair of triple-GEM chambers"

## **Office Based Controlling**

![Placeholder](../assets/images/shifterguide/general-guides/DCS_abbreviations.PNG){ loading=lazy : style="width:543px"} 
