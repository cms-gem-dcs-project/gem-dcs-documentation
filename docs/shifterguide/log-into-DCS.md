# **Logging into DCS**

!!! tip "After selecting the option **"GEM DCS"** from the list in the terminal, the user will be able to access the DCS main interface."
	
!!! warning "**Users must have privileges assigned to them in order to perform certain tasks.** If not the system will automatically restrict the options that are above the limit of privileges for the user that logged into the system."

1.	Click on the key icon select the **"Log in"** option.
	
	![Placeholder](../assets/images/shifterguide/general-guides/login_key.PNG){ loading=lazy : style="width:430px" } 

2.	Provide user credentials in the dialog box and click **"OK"**.

3.	If successfully logged in, then the user name and the role would appear in the field beside the key icon.


	
