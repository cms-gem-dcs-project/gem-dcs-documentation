# **What is DCS?**

!!! danger "DCS = Detector Control System"

![binbash-leverage-tf](../assets/images/shifterguide/DCS_image.png#right "GEm_DCS"){: style="width:320px"}

Detector is too complex and it is not possible to operate a detector by a small crew of experts. 
A Detector has to run without interruption for many years, except in long shutdown periods.
In order to make it possible to operate the detector easily, the name **"Detector Control System" (DCS)** comes into the picture. 

DCS system of the GEM GE1/1 station has been developed based on a **"Supervisory Control and Data Acquisition" (SCADA)** software referred to as **"Wincc OA"**.

!!! tip "DCS Purpose"
    * **Continuous and safe operation of the detectors** 
    * **Voltage (HV/LV) controlling and monitoring**
	* **Monitoring of gas system and environmental parameters**
	* **External system communication**

!!! info "The 5 Main Sections of the GEM DCS"
    * **Standard HV + LV control and monitoring** 
    * **Finite State Machine (FSM)**
	* **Gas and environmental parameters monitoring**
	* **Radiation monitoring (RadMon)**
	* **LHC, Magnet, DSS, Temperature monitoring**

