# **Reacting to an Alarm Notification**

Make use of following points in order to react accordingly to an alarm notification.

* Connect to the DCS and open the alarm screen.
* Identify the variable involved. 
* Understand if the variable is in a warning state or in an error state. 
* Act properly refering the DCS Twiki.

!!! warning "If unsure or unable to understand the alarm or the reason for the notification, it is recommended to contact an [expert](../contact.md)."	

A list of common errors and a short description of the actions to be taken is available in [“Common Errors and Actions”](../shifterguide/common-errors-and-actions.md) section.

Following are a few examples for alarm notifications, explanation for the recieved notification and the course of actions to be taken after identifying the problem that triggered the alarm. 

??? example "Example 1 :  Standard HV channel in error"
	??? failure "**You received this message :**"
		```
			Channel Error cms_gem_dcs_1:CAEN/GEM_CAEN_HV/board02/channel001.actual.OvC Channel Error (bool alert activated);
			Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
		```
		
	??? info "**What does it mean?**"
		* `cms_gem_dcs_1:CAEN/GEM_CAEN_HV/board02/channel001` : Name of the channel in error (From the twiki, HV channel of GEMINI27-Layer2)
		* `.actual.OvC` : Type of error (In this case, OvC or Overcurrent)
	
	??? bug "**What to do?**"
		1. 	Open the DCS and search for the right chamber.
		2.	Which is the status of the HV channel?
			
			**Case 1 : The OvC can be transient - The channel returns to the normal condition.**
					
			**Actions :**
		
			1.	Check the alarm screen where the OvC error should have direction WENT.
			2.	Acknowledge it.
			
			**Case 2 : If the OvC is not transient - The channel trips.**
		
			**Actions :**
		
			1.	Check the alarm screen where both the OvC error and the Trip error should have direction CAME.
			2. 	Check if the gas is correctly flowing into the chamber (In the 'GAS' tab, go with the mouse over the correct flowcell rectangle.
				* If the gas is NOT-OK : Should have received a notification regarding that too.
				* If the gas is OK : Try to restart the HV on the chamber.
					* From the CAEN tab, search for GEMINI27-Layer2, press ON HV and wait until the ramping up is complete.
					* If the chamber remains ON, acknowledge all the alarms.
					* If the chamber trips again, call the expert.

??? Example "Example 2 : LV channel in error"
	??? failure "**You received this message :**"
		```
			Channel Error
			cms_gem_dcs_1:CAEN/GEM_CAEN_LV/branchController00/easyCrate0/easyBoard17/channel003.actual.unplugged Channel Error (bool alert activated);
			Channel Error
			Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
		```

	??? info "**What does it mean?**"
		* `cms_gem_dcs_1:CAEN/GEM_CAEN_LV/branchController00/easyCrate0/easyBoard17/channel003` : Name of the channel in error (From the Twiki, LV channel GEB/VFAT of GEMINI01–Layer2)
		* `.actual.unplugged` : Type of error (OvC = Unplugged)
	
	??? bug "**What to do?**"
		**Case 1 : When the system is completely restarted with the LV ON button on the main panel.**
		
		**Actions :**
		
		1.	Open the DCS and search for the right chamber.
		2.	Which is the status of the LV channel?
			* The channel should be in error (in red), probably also the other two channels of the same chamber are in error.
		3. 	Check the alarm screen : The error should have the direction CAME.
		4.	Clear the LV alarms with the button on the DCS main panel.
		5.	Try to restart the LV.
			* From the CAEN tab, search for GEMINI01-Layer2, press ON LV and wait until the LV is ON
			* If it stays ON, acknowledge the alarms.
			* If it goes in error again, call the expert.
			
		**Case 2 : When the MAO goes off and as a result all the LV channels of all the chambers go in unplugged error all together.**
		
		**Actions :**
		
		1. 	Clear the LV alarms with the button on the main panel.
		2. 	Try to restart the MAO.
			* If the MAO stays ON, follow the procedure above for all the chambers.
			* If the MAO goes OFF again, call the expert.
			
??? Example "Example 3 : Gas Flowcell in error"
	??? failure "**You received this message :**"
		```
			Alert: check the input flow cms_gem_dcs_1:CMSGEM_Di_FE6102Ch4.Value= 0.199999999999 (multi range alert triggered);
			Message sent from system cms_gem_dcs_1: @ DCS-S2F16-07-16
		```
	
	??? info "**What does it mean?**"
		* `cms_gem_dcs_1:CMSGEM_Di_FE6102Ch4.Value= 0.199999999999` : Name of the channel in error (From the Twiki, Flowcell Ch4 GEMINI 29-30).
		* `check the input flow + CMSGEM_Di_FE6102Ch4` : Type of error (In this case wrong input flow on that channel)
	
	??? bug "**What to do?**"	
		1. 	Open the DCS and search for the right flowcell.
		2. 	Go with the mouse over it (or open the Gas Settings table) and understand which is the current input gas flow value
			
			**Case 1 : If the gas flow is back to the normal value.** 
			
			**Actions :**
			
			1.	It was only a glitch in the DIP readings. 
			2. 	Acknowledge the alarms.
		
			**Case 2 : If it is not back to the normail value, potentially a gas problem.**
		
			**Actions :**
			
			1.	Understand if it is a warning (yellow) or an alert (red).
			2.	If it is a warning, still have gas flow in the chambers, wait a bit to see if the normal flow is restored. 
			3.	If it is not, call the GEM DCS expert or the GEM GAS expert to understand if this gas flow can be accepted for a while.
			4. 	If it is an error and not a fluctuation (i.e. in 2-3 min is not returned to a safe range), immediately contact the GEM DCS expert or the GEM GAS expert! 
			5.	Arrange with them the next step, which might be calling the CMS GAS expert (or the Gas Piquet).
				* If your flow (or pressure) is really going to zero and you don’t manage to contact the GEM expert, call directly the CMS GAS expert (or Gas Piquet).
			6.	If the problem is solved, just acknowledge the alarm. **In case the problem is not solved the chambers should be switched OFF.**
		
!!! info "For all alerts, at the end of the intervention, write a detailed ELOG, with a description of the problem and the actions that have been taken!"
