# **CAEN Module Status Monitor** 

CAEN module status monitoring area provides information on employed CAEN modules in GEM detectors. 

!!! tip ""
	![Placeholder](../assets/images/shifterguide/status-monitors/CAEN_status.PNG){ loading=lazy : style="width:528px" , align=right }  
	
	**HV Status**
	
	- [x] HV Main Frame status
	
	**LV Status**
	
	- [x] LV Main Frame status
	- [x] Branch controller status
	- [x] Power supply status 
	- [x] Crate status 

## **HV Status** 

HV Main Frame Status area provides information on all 4 HV Main Frames assigned for GEM detectors at End Caps for the HV power distribution achieved by Super Chambers. 

!!! info "**HV Main Frame Status**"
	![Placeholder](../assets/images/shifterguide/status-monitors/CAEN_hv_MF_status.PNG){ loading=lazy : style="width:95" , align=left }  

	A single click on a HV Main Frame cell results in a popup panel that displays the HV Main Frame name, on / off status of the selected HV Main Frame, information on AC and primary power supply status, communication status, fan status and speed  and faulty states if any exists.

!!! tip "Main Frame status, AC status and primary power supply status for the selected GEM foil will be presented with the relevant color based on defined ranges for the color code." 

!!! warning "If the system is in a faulty state the relevant faulty type will be appointed with a red color to distinguish and necessary notifications will be sent to relevant parties."

!!! failure "Refer ["Common Errors and Actions"](../shifterguide/common-errors-and-actions.md) for common faulty states, errors and actions to be taken in case of such failures."

## **LV Status** 

The connection of LV Main Frame to Branch Controllers which then extends to Easy Crates via MAO are indicated in the LV status monitoring area.

!!! info "**LV Main Frame Status**"
	![Placeholder](../assets/images/shifterguide/status-monitors/CAEN_lv_MF_status.PNG){ loading=lazy : style="width:100" , align=left} 

	LV Main Frame Status area provides information on all 4 LV Main Frames assigned for GEM detectors at 2 End Caps considering the LV power distribution achieved by the division of Super Chambers. 

	A click on the Main Frame box results in a panel that displays the LV Main Frame, on/off status, primary power supply status, communication status, fan status and faulty states if it exists.
	
!!! info "**Branch Controller Status**"
	![Placeholder](../assets/images/shifterguide/status-monitors/CAEN_lv_branchcontroller.PNG){ loading=lazy : style="width:100" , align=left} 
	
	This area provides information on the Branch Controller status and all Branch Controllers associated with the detector are indicated. A single click on a single Branch Controller box results in a popup panel that states the on/off state.
 
!!! info "**48V Power Supply (MAO) Status**"
	![Placeholder](../assets/images/shifterguide/status-monitors/CAEN_lv_power.PNG){ loading=lazy : style="width:100" , align=left} 

	A single click on a power converter box results in a popup panel that displays on/off status information on the power converters for a selected LV Branch Controller board.

!!! info "**Crate Status**"
	Status of Easy Crates that facilitate LV power supplies are indicated in the area and a single click on an Easy Crate box results in a popup panel that provides information on the Crate status.

!!! tip "Main Frame status, AC status and primary power supply status for the selected LV component will be presented with the relevant color based on defined ranges for the color code." 

!!! warning "If the system is in a faulty state the relevant faulty type will be appointed with a red color to distinguish and necessary notifications will be sent to relevant parties."

!!! failure "Refer ["Common Errors and Actions"](../shifterguide/common-errors-and-actions.md) for common faulty states, errors and actions to be taken in case of such failures."

--8<-- "docs/includes/abbreviations.md"
