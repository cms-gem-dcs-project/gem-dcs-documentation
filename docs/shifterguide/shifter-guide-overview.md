# **Shifter's Guide Overview**

The **Shifter's Guide** intends to provide an integrated and comprehensive documentation source for contributing to the controlling of GEM DCS as an operator. The guide is handled by the same group that built the current DCS for GEM detectors.

The main DCS interface system has several associated sections and buttons, each of which allows effortless visualization and controlling of the system and is divided into 2 regions. The **Tab Region** with the option to select and the **Common Region** visible to all views (including all tab options).  

!!! tip "Upon starting the DCS, all sections are disabled and it is expected of the user to login before operating the system." 

!!! warning "Depending on the level of privileges users might have restrictions on applying changes to the system. For GEM_Expert all of fuctionality is activated and for GEM_User limited fuctionality is given." 

--8<-- "docs/includes/abbreviations.md"
