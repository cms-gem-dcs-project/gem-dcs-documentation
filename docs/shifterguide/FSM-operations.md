# **FSM Operations**

!!! info "User must be the owner of the FSM to do the following steps. If not, ask from the current owner to release FSM in central or take FSM back local for you."

!!! tip "<span style="color:red">For any reason FSM should not be without an owner.</span> Communicate with the technical shifter in CMS control room (LHC-Point5), when releasing the FSM in central or taking it back to local."
	
## **Inclusion of GEM FSM in CMS (Release FSM in Central)**

### **Standard Method to Release FSM in Central**

1. 	First inform the technical shifter of the intention to release the FSM in central. 

2.	Click on the button **"Release Control"** and confirm the release from local.
	
	![Placeholder](../assets/images/shifterguide/FSM/release_button.PNG){ loading=lazy : style="width:385px" }
	
3.	The technical shifter can see (in the central DCS screen) about the released FSM and he/she will include the GEM FSM in CMS (central).
	
	!!! info "A gray color crossed and opened padlock in **“GEM_Logical”** node will be visible if the node is properly  in the **“Exclude&LockOut”** state."	
	
	![Placeholder](../assets/images/shifterguide/FSM/in_central.PNG){ loading=lazy : style="width:402px" }
	
4. 	The FSM is in Central and the long tab will indicate the current owner, in this case, the technical shifter !

### **Alternate Method to Release FSM in Central**

1. 	First inform the technical shifter of the intention to release the FSM in central. 

2. 	Click on the padlock in **“GEM_Logical”** node **(A)**.

	![Placeholder](../assets/images/shifterguide/FSM/exclude_lock_out.PNG){ loading=lazy : style="width:401px" }
	
3. 	Click on the **“Exclude&LockOut”** button **(B)**.

4. 	The technical shifter can see (in the central DCS screen) about the released FSM and he/she will include the GEM FSM in CMS (central).
	
	!!! info "A gray color crossed and opened padlock in **“GEM_Logical”** node will be visible if the node is properly  in the **“Exclude&LockOut”** state."	
		
	![Placeholder](../assets/images/shifterguide/FSM/in_central.PNG){ loading=lazy : style="width:402px" }
	
5. 	Go inside the **“GEM_Logical”** node, observe the red colored locked padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes which means that someone else owns these FSM nodes.
	
	![Placeholder](../assets/images/shifterguide/FSM/node_central.PNG){ loading=lazy : style="width:398px" }

6. 	Now the FSM is in Central and the owner at the moment in this case is the technical shifter !
	
## **Exclusion of GEM FSM from CMS (Take FSM in Local)**

### **Standard Method to Take FSM in Local**

1. 	Inform the technical shifter of the intention to take the FSM in local. 

2.	Click on the button **"Take Control"** and confirm the release from local.
	
	![Placeholder](../assets/images/shifterguide/FSM/take_button.PNG){ loading=lazy : style="width:384px" }
	
3.	The padlock will become locked and color will be changed into green. At the same time, the technical shifter can see that you have taken the FSM.
		
5. Now the FSM is in Local and the green tab above the buttons will indicate you as the owner !

### **Alternate Method to Take FSM in Local**

1. 	Inform the technical shifter of the intention to take the FSM in local and wait until technical shifter releases the GEM FSM from CMS. 

2. 	If checked inside the **“GEM_Logical”** node, the gray color opened padlocks in **“GEM_Endcap_Minus”** and **“GEM_Endcap_Minus”** nodes will be visible.
	
	![Placeholder](../assets/images/shifterguide/FSM/node_released.PNG){ loading=lazy : style="width:391px" }
	
3. 	Click on the padlock in **“GEM_Logical”** node **(A)**.
	
	![Placeholder](../assets/images/shifterguide/FSM/unlock_out_include.PNG){ loading=lazy : style="width:426px" }
	
4. 	Click on the **“UnlockOut&Include”** button **(B)** and then the padlock will become locked and change into green color. At the same time, the technical shifter can see that you have taken the FSM.
		
5. Now the FSM is in Local !

	??? failure "If clicked on **“UnlockOut&Include”** and recieved the following message, it means the FSM is still in central and the technical shifter has not released the FSM yet."
		![Placeholder](../assets/images/shifterguide/FSM/fsm_error.PNG){ loading=lazy : style="width:461px" }
		
		**If this happens,**
		
		- Click **“Dismiss”** to close the message.
		- Wait until technical shifter release the FSM or ask again to release the FSM. 
		- Then click on the padlock in **“GEM_Logical”** node and click on the **“Exclude&LockOut”** button. 
		- Click again on the padlock in **“GEM_Logical”** node and click on the **“UnlockOut&Include”** button.

## **FSM Actions**

### **Sending Actions through the FSM**

1.	Click on the downward arrow in the field **'State'** of the **'System'** or the **'Sub-system'** that expects to send the action through.

2.	Select the action expected to be sent to other nodes from the available action options in the dropdown menu. 

	??? info "List of Possible Actions"
		!!! note ""
			<span style="color:DodgerBlue">**High Voltage Nodes**</span>
		
			![Placeholder](../assets/images/shifterguide/FSM/HV_actions.PNG){ loading=lazy : style="width:411px"} 

		!!! bug ""
			<span style="color:LightCoral">**Low Voltage Nodes**</span>
			
			![Placeholder](../assets/images/shifterguide/FSM/LV_actions.PNG){ loading=lazy : style="width:409px"} 

		!!! tip ""
			<span style="color:LightSeaGreen">**Gas Nodes**</span>
		
			![Placeholder](../assets/images/shifterguide/FSM/Gas_actions.PNG){ loading=lazy : style="width:691px"} 

### **Sending Actions through the FSM (Lower Nodes)**

1.	Double click on the node to expand the lower nodes in a different tab.

2.	Click on the downward arrow in the field **'State'** of the **'Sub-system'** that expects to send the action through.

3.	Select the action expected to be sent to other nodes from the available action options in the dropdown menu. 
	
	??? info "List of Possible Actions"
		!!! note ""
			<span style="color:DodgerBlue",font-size:50px>**High Voltage Nodes**</span>
		
			![Placeholder](../assets/images/shifterguide/FSM/HV_actions.PNG){ loading=lazy : style="width:411px"} 

		!!! bug ""
			<span style="color:LightCoral">**Low Voltage Nodes**</span>
			
			![Placeholder](../assets/images/shifterguide/FSM/LV_actions.PNG){ loading=lazy : style="width:409px"} 

		!!! tip ""
			<span style="color:LightSeaGreen">**Gas Nodes**</span>
		
			![Placeholder](../assets/images/shifterguide/FSM/Gas_actions.PNG){ loading=lazy : style="width:691px"} 

## **Basic FSM Rules**

!!! warning "The direct transition from OFF to ON for any HV node(and as a consequence for the GEM_Endcap node) is not permitted."

!!! tip "Lower states are always predominant, i.e. if there are 9 chambers ON with 1 in STANDBY, then the top node state will be STANDBY and the predominance of lower nodes will stay the same until the majority is not defined."

??? info "Complete set of rules defined for High Voltage, Low Voltage and Gas System nodes"
	!!! note ""
		<span style="color:DodgerBlue">**High Voltage Child Nodes**</span>
	
		=== "HV Chamber Children (GEMINI_Layer)"
			* The **HV_Chamber** has a single child of type **HVChannel**

		=== "GEMINI_HV Children"
			* The **GEMINI_HV** has 2 children of type **HVChamber**
		
		=== "GEM_HV Children"
			* The **GEM_HV** has 5 children (for the Slice Test) of type **GEMINI_HV**
		
		<span style="color:DodgerBlue">**Rules for High Voltage Nodes**</span>
		
		![Placeholder](../assets/images/shifterguide/FSM/HV_rules.PNG){ loading=lazy : style="width:554px"} 

	!!! bug "" 
		<span style="color:Crimson">**Low Voltage Nodes**</span>
	
		<span style="color:LightCoral">**LV Children Nodes**</span>
		
		=== "LV Chamber Children (GEMINI_Layer)"
			* The **LV_Chamber** has 3 children of type **LVChannel**
			
		=== "GEMINI_LV Children"
			* The **GEMINI_LV** has 2 children of type **LVChamber**
		
		=== "GEM_LV Children"
			* The **GEM_LV** has 5 children (for the Slice Test) of type **GEMINI_LV**
		
		<span style="color:LightCoral">**Rules for LV Nodes**</span>
		
		![Placeholder](../assets/images/shifterguide/FSM/LV_rules.PNG){ loading=lazy : style="width:551px"} 

	!!! tip ""
		<span style="color:Teal">**Gas System Nodes**</span>
	
		<span style="color:LightSeaGreen">**Gas System Children Nodes**</span>
		
		* The **GEM_ENDCAP_Minus** chamber has 3 children of type **GEM_HV**, type **GEM_LV** and type **Gas_System**
		
		<span style="color:LightSeaGreen">**Rules for Gas Nodes**</span>
		
		![Placeholder](../assets/images/shifterguide/FSM/Gas_rules.PNG){ loading=lazy : style="width:690px"} 


