# **Detector Protection System**

**Detector Protection** is a system used to move the detectors in a safe state when particular external conditions related to LHC or the magnet status, are satisfied. 

!!! tip "A typical example of Detector Protection input condition is the beam injection in the LHC. When the input condition is satisfied, the Detector Protection is fired."

## **When does the Detector Protection get fired for GEM?**

In the present, GEM has only 1 situation in which the **Detector Protection** is used, referred to as the **beam injection**. It is possible to view when the **Detector Protection** is expected to be fired via the **Automation Matrix**. 

!!! info "Refer the following PROTON PHYSICS case."
	![Placeholder](../assets/images/shifterguide/general-guides/proton_physics_auto.PNG){ loading=lazy : style="width:741px" } 
 
## **How is the Detector Protection configured for GEM?**

When the **Detector Protection** is fired, the GEM goes into the PROTECTED STANDBY mode. 

!!! info "The difference between PROTECTED STANDBY and STANDBY is that with the former the DPs are locked and cannot change for errors if occured."

??? tip "If the detector is in the PROTECTED STANDBY mode following changes and conditions would occur."
	* Vset is set to the voltage used for the STANDBY state.
	* Vset is locked, i.e. it cannot be changed.
	* ON/OFF works only in the OFF direction, i.e. it is still possible to turn OFF the detectors, but if the detectors where OFF when the input condition was fired, they remain OFF and are locked OFF.

## **How to check if the Detector Protection fired?**

* Option 1 : Check the **Beam Machine Mode** and the **Automation Matrix**.
* Option 2 : Check the local GEM DCS screen, to see if it indicates the state as fired.
* Option 3 : Check directly from the Central DCS screen.
	