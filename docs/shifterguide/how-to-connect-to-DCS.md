# **Connecting to the DCS**

The user might have to use one of the following methods to log in to the DCS depending on the Operting System and the location before accessing DCS. 

=== "Windows Operating Systems"

    ??? info "From Inside CERN" 
		* Remotely connect to `cerntscms` with CERN user credentials. 
	
	??? info "From Outsde CERN"
		* Remotely connect to `cerntsnew` with CERN user credentials.
		* Remotely connect to `cerntscms` with CERN user credentials.

=== "Linux Operating Systems"

	??? info "From Inside CERN" 
		* Type `rdesktop -g 100% -a16 -u myUser -d CERN cerntscms.cern.ch` in a terminal.
	
	??? info "From Outsde CERN"
		* Type `ssh -X -Y myUser@lxplus.cern.ch and rdesktop -g 100% -a16 -u myUser -d CERN cerntscms.cern.ch` in a terminal.

=== "Mac Operating Systems"

	??? info "From Inside CERN" 
		* Install remote desktop client for Mac.
		* Remotely connect to `cerntscms.cern.ch` with CERN user credentials.
	
	??? info "From Outsde CERN"
		* Type `ssh -L 50000:cerntscms.cern.ch:3389 userName@lxplus .cern.ch` in a terminal.
		* Open the remote desktop application and type `localhost:50000` in the field.

Select the option **"GEM DCS"** from the list in the terminal.

![Placeholder](../assets/images/shifterguide/general-guides/DCS_terminal.PNG){ loading=lazy : style="width:577px"} 

