# **DCS Roles**

The GEM DCS has 2 roles associated with the system, each of which has a set of predefined privilages. As such users might get notifications on restrictions if the role has no clearance for required privileges for certain tasks. 

* A **GEM_Expert** has full access to the system and can change HV, LV, alarm ranges, etc. settings completely.

* A **GEM_Operator** only has access to check the status of the system, switch ON/OFF the HV and LV without changing the settings, receive and acknowledge alarms.

