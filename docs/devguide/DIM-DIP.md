# **DIP and DIM Communications**


## **DIP Communication** 

DIP is a communication system that allows small amounts of soft real time data to be exchanged between loosely coupled heterogeneous systems. 

!!! warning "DIP is currently only supported on Windows 64-bits and Linux 64-bits but there is no support for Mac OSX."

??? info "DIP service is composed with several important and mandatory parts."
	* A Central Name Server : To provide the list of available publications
	* An API : To allow publication and receival of information.
	* A WinCC OA API Manager : To allow publication and receival of DIP data in WinCC OA.
	* A LabVIEW extension : To allow publication and receival of DIP data in NI LabVIEW.

### **DIP Browser**

This system allows the users to access external data via the option **'CMS DIP'** in the terminal server **'DB Editor and Navigator'** tool.

![Placeholder](../assets/images/devguide/Special/terminal_DIP.PNG){ loading=lazy : style="width:578px"} 

The DIP option can simply be accessed by clicking on it which will lead to a Java based panel with a header bar indicating **"DIP Browser 0.0.0"** (note that 0.0.0 represents the version). 

![Placeholder](../assets/images/devguide/Special/DIP_inside.PNG){ loading=lazy : style="width:494px"} 

Clicking the **">>"** icon at the end of the tab area with the DNS of the system will provide the user with a folder view of published data. 

### **DIP Communication Bridge**

In order to access external data, it is necessary to instigate a communication line from the external system to the user system such that the line would act as a communication bridge. 

!!! info "DIP is the system that ensures this communication bridge maintaining an internal structure consisting of publications, publishers, subscriptions and subscribers."
	![Placeholder](../assets/images/devguide/Special/DIP_architecture.PNG){ loading=lazy : style="width:568px"} 

	<span style="color:DodgerBlue">**Blue Arrows**</span> : Action is required by the writer of the Publisher or Subscriber in order for the information to flow. 
	
	<span style="color:OrangeRed">**Red Arrows**</span> : Information flow between components is handled by DIP.

Definitions for the components in the structure diagram are as follows.

!!! info ""
	<span style="color:DeepSkyBlue">**Data Source**</span> 
	
	Data source that is to be sent via DIP which may be either internal or external to the DIP server. The DIP server is responsible for accessing the data source and writing it out to DIP through the <span style="color:Green">**Publication**</span> when datapoints are updated.

!!! check ""
	<span style="color:Green">**Publication**</span> 
	
	Represents a datapoint published in DIP. The writer of the server must write the code that obtains the data from the <span style="color:DeepSkyBlue">**data source**</span> and writes it into the <span style="color:Green">**publication**</span> object. 
	
!!! quote ""
	<span style="color:Gray">**Subscrption**</span> 
	
	An object given to the client by DIP when the client subscribes to a publication which allows the client to request recently published value of the <span style="color:Green">**publication**</span> or unsubscribe.

!!! quote ""
	<span style="color:Gray">**Subscrption Listener**</span> 
	
	This is the user of the client side and the implementer of the client must provide a code in the <span style="color:Gray">**Subscrption Listener**</span> to perform required tasks with the data as it arrives.
	
### **DIP Configuration File in DCS**

In the development platform the framework component, **'CMSfwDIP'** supports establishing the communication bridge. 

!!! tip "Generally the external groups associated in the DCS production," 
	* Creates a **'clientConfig'** XML file allowing easy access to their system data. 
	* A client subscribes to the publication by providing DIP with the publication name. 
	* The mapping is done via the **'FwDipConfig'** where the published data are cached inside **'serverConfig'** and accessible from **'clientConfig'**.  

### **FOS Sensor Data and DIP Configuration**

In short Super Chambers of the detector there are 2 temperature sensors installed for each layer referred to as FOS. The data collected through FOS sensors are published in a different system.

The FOS data folder for the GEM poject can be accessed by expanding the DIP folder and then the CMS folder. Inside the FOS folder the datasheets for each layer of each short Super Chamber exists, all of which contain float datapoints. 

![Placeholder](../assets/images/devguide/Special/DIP_datapoint.PNG){ loading=lazy : style="width:462px"} 

These data should be accessible via the DCS but is problematic due to the fact, those published data are in a seperate system with no direct connection to DCS and there is no support configuration from publisher side.

To access published data from FOS, the developer himself should configure the system to read data from the external system. This is accomplished by writing a configuration script in the production system, and then running the relevant manager.

## **DIM Communication**