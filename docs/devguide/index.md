# **Developer's Guide Overview** 

This **Developer's Guide** intends to provide a comprehensive resource for contributing to the construction of GEM DCS regarding experienced and inexperienced developers. The guide is handled by the same group that built the current DCS for GEM detectors. 

The content of the guide offers step by step instructions on several important topics, from the basic steps needed to get set up to proceed with more advanced mechanisms once the user knows the basics. 

DCS system of the GEM GE1/1 station has been developed based on a **"Supervisory Control and Data Acquisition" (SCADA)** software referred to as **"Wincc Open Architecture"** with a modified **"JCOP Framework"**.