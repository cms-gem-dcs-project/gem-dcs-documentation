# **Configuring User Privileges**

All DCS users do not have the privilege to get all the features from administrator to low level associated with the control system. 

!!! danger "The default user is ‘root’ and it is not usually sufficient for developers. It is recommended to define a user with all privileges unless otherwise specified for the development purposes."

Follow the steps given below to define a user with administrator (or selected if specified) privileges.

1.	Click on the **"Jcop Framework"** tab, select **"Access Control"** from the dropdown menu and select the option **"AC Toolbar"** from the extended menu.
	
	![Placeholder](../assets/images/devguide/Special/ACtoolbar.PNG){ loading=lazy : style="width:480px" } 

2.	Right click on the key icon of the popup dialog box and select **"Administrator"** option.
	
	![Placeholder](../assets/images/devguide/Special/user_options.PNG){ loading=lazy : style="width:382px" } 

3.	Select **"Domains"** and click on the **"Add"** button of the **"Access Control Domains"** panel.
	
	![Placeholder](../assets/images/devguide/Special/user_domain.PNG){ loading=lazy : style="width:672px" } 

4.	Fill in the fields Domain name, Domain full name, if necessary include a Domain comment and click the **"Create"** button.
	
	![Placeholder](../assets/images/devguide/Special/user_domain_pop.PNG){ loading=lazy : style="width:582px" } 

	!!! info "If the domain was successfully created it will be visible in the domain list of ‘Access Control Domains’ panel."

5.	Right click on the key icon of the popup dialog box and select **"Administrator"** option.

6.	Select **"Groups"** and click on the **"Add"** button and select **"Create New Group"** option of the **"Access Control Groups"** panel.
	
	![Placeholder](../assets/images/devguide/Special/user_group.PNG){ loading=lazy : style="width:672px" } 

7.	Fill in the fields Group name, Group full name, if necessary include a Group description and click the **"Edit"** button of **"Granted Access Rights"** section.
	
	![Placeholder](../assets/images/devguide/Special/user_group_new.PNG){ loading=lazy : style="width:582px" } 

8.	Select the previously created Domain name, move all items using **">>"** icon unless otherwise specified, to the other side and click the **"Create"** button.
	
	!!! tip "If otherwise specified on domain privileges use ">" to move selected items."
	
	![Placeholder](../assets/images/devguide/Special/user_group_pop.PNG){ loading=lazy : style="width:672px" } 

	!!! info "If the group was successfully created it will be visible in the domain list of ‘Access Control Groups’ panel."

9.	Right click on the key icon of the popup dialog box and select **"Administrator"** option.

10.	Select **"Users"** and click on the **"Add"** button of the **"Access Control Users"** panel.
	
	![Placeholder](../assets/images/devguide/Special/user_users.PNG){ loading=lazy : style="width:672px" } 

11.	Fill in the fields providing a user name and a password and click the **"Edit"** button of the **"Group Membership"** section.

12.	Select the previously created Group name, move all items using **">>"** icon unless otherwise specified, to the other side and click the **"Create"** button.
	
	!!! tip "If otherwise specified on domain privileges use ">" to move selected items."
	
	![Placeholder](../assets/images/devguide/Special/user_users_pop.PNG){ loading=lazy : style="width:672px" } 

	!!! info "If the user was successfully created it will be visible in the domain list of ‘Access Control Users’ panel."

13.	Click on the key icon to log into the system, provide credentials and log in using the popup dialog box. 

	![Placeholder](../assets/images/devguide/Special/def_user.PNG){ loading=lazy : style="width:385px" } 
	
	!!! check "The user will be able to perform actions validated by set privileges."

--8<-- "docs/includes/abbreviations.md"
