# **WinCC OA Licence** 

## **Demo Licence**
 
If the user does not have an approved licence for WinCC OA from CERN then the user is working employing the default demo licence. 

!!! warning "The demo licence only lets the user work for 30 minutes before the system automatically shuts down. The user will be warned with the below message at the beginning."
	![Placeholder](../assets/images/devguide/WinCCOA/no_license.PNG){ loading=lazy : style="width:412px" } 

## **CERN Approved License**
 
In order to obtain a CERN approved WinCC OA license the user must request through the WinCC OA application itself using personal CERN credentials.

### **Applying for CERN WinCC OA License** 

1.	Go to the **"Project Administrator"** panel of the installed WinCC OA application.
2.	Click on the **"Get Hardware Code"** button.
	
	![Placeholder](../assets/images/devguide/WinCCOA/PA_get_hardware_code.PNG){ loading=lazy : style="width:626px" } 

3.	Fill the user information and machine information fields of the license request form.

	![Placeholder](../assets/images/devguide/WinCCOA/license_form.PNG){ loading=lazy : style="width:650px" } 

4. 	Click on the button next in the **"License Request by Email"** section.

	!!! info "In case of license requests via local machines gives an error when requested by email. In such cases click on the button next in the "License Request by Web" section to proceed."
	
5. 	If the request was successful a confirmation dialog box will appear. User will recieve an email on the request.

	![Placeholder](../assets/images/devguide/WinCCOA/license_requested.PNG){ loading=lazy : style="width:650px" } 
	
### **Activating the License** 

1.	Cut the received licence as the instructions in the email indicate.
2.	Create a text file in the name <span style="color:blue">**shield.txt**</span> and paste the required section in the text file. 
3.	Save the license file (shield.txt) at the root of the software installation directory. 
	
	!!! info "WinCC OA requires a license file called 'shield' and if such a file already exists, replace it with the newly received licence."
	
	!!! tip "If the user has multiple versions of WinCC OA installed, ensure the licence file is copied to the correct version."

	!!! warning "Do not edit the license file and in case of an invalide license error message check the FAQ provided in the email or contact support teams via the provided links."
	
	!!! danger "A license is valid only for a period of 1 year and after this duration user must request another license. If the system settings were changed within the mentioned duration user will have to request for a new license."

--8<-- "docs/includes/abbreviations.md"
