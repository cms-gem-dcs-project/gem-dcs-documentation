# **Local Setting Up of the Database**

1.	Visit the git repository for CERN via https://cmsonline.cern.ch using CERN credentials.

2.	Follow the steps 1 through 4 of ["Setting Up GEM DCS Components"](../devguide/setting-DCS-components.md) section.

3.	Open CMS_GEM_GE11_UI downloaded component, move inside the subdirectory **"dplist"** and copy the directory named **"CMS_GEM_GE11_UI"** inside it. 

4.	Move to the project directory the user is currently working with, select the subdirectory with the same name i.e. **"dplist"** and paste the copied directory.

5.	Repeat this for **"panels"** and **"pictures"** subdirectories by copying the directory named **"CMS_GEM_GE11_UI"** inside them and paste in the relevant **"panels"** and **"pictures"** directories of the project directory.

	![Placeholder](../assets/images/devguide/JCOPfw/UI_components_DB.PNG){ loading=lazy : style="width:624px" } 

6.	Repeat steps 3 through 5 for other downloaded components too considering the information provided in the information below.
	
	!!! help "Subfolders that should be considered in the procedure for each downloaded component are as follows." 
		- [x] CMS_GEM_GE11_CAEN			: dplist, scripts
		- [x] CMS_GEM_GE11_DetectorProtection	: dplist, panels, scripts
		- [x] CMS_GEM_GE11_FSM			: dplist, panels, scripts
		- [x] CMS_GEM_GE11_GAS			: dplist, panels, pictures
		- [x] CMS_GEM_GE11_UI			: dplist, panels, pictures
	
	!!! info "There is a directory named ‘libs’ inside ‘Scripts’ subdirectories of components. Copy the directory inside the ‘libs’ directory and paste it inside the ‘libs’ directory inside the ‘Scripts’ directory of the project."
	
7.	Click on the **"SysMgm"(System Manager)** icon  from WinCC OA main toolbar and select the option **"Database"** from the new window.

	![Placeholder](../assets/images/devguide/JCOPfw/SysMgm_DB.png){ loading=lazy : style="width:652px" } 

8.	Select **"ASCII Manager"** from the resultant window.

	![Placeholder](../assets/images/devguide/JCOPfw/SysMgm_ASCII.PNG){ loading=lazy : style="width:651px" } 

9.	Select the **"Import"** radio box **(A)** and click on the folder icon **(B)** to provide a .dpl file.

	![Placeholder](../assets/images/devguide/JCOPfw/SysMgm_Import.PNG){ loading=lazy : style="width:650px" } 

10.	Click on **"Start"** button **(C)** and confirm the process in the popup dialog box.
	
	![Placeholder](../assets/images/devguide/JCOPfw/SysMgm_ASCII_confirm.PNG){ loading=lazy : style="width:230px" } 

11. Repeat steps 3 through 10 for all components i.e. UI, GAS, FSM, Detector Protection and CAEN. 

12. In each case the user will be notified by a popup log that provides information of the process.

	![Placeholder](../assets/images/devguide/JCOPfw/SysMgm_ASCII_log.PNG){ loading=lazy : style="width:650px" } 

--8<-- "docs/includes/abbreviations.md"
