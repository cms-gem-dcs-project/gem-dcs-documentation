# **FSM Navigation for Errors**

## **Vocabulary Mismatch Adjustments**

Vocabulary mismatch between the states defined for a child and the states understood by a parent is a very common mistake  when setting up an FSM. 

!!! tip "The parent CUs must be introduced to the fact that children can take different states by refining definitions of the CU Type."

!!! danger "In the case of the example, if the MuonSubSystem was left switched ON, the MuonCu will still be in NOT_READY state. Why is this?" 
	* The MuonSubSystemPower (DU) is reporting a state 'ON'. 
	
	* Unfortunately the MuonCu (CU) does not know how to deal with the state 'ON' coming in from its DU child.
	
	**If the MuonCuType and TrackerCuType were copies of DcsCuType in content,**
	
	* DcsCuType was defined to itself be able to adopt 'READY', 'NOT_READY' or 'ERROR' states, similarly all of the default definitions concerning any of the states that its children may adopt. 
	* In fact, those states might be appropriate for DcsCuType, but not for MuonCuType nor TrackerCuType.

### **Refine the Parent Vocabulary of Childrens’ States**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.
	
2.	Click on **"Create/Configure FSM Object Types"** button.

3.	In the **"FSM Types"** panel, double click on the CU Type that needs to be refined. 

4.	Consider the **"When List"** at the bottom of the **"SMI_Object_States"** panel and observe where the vocabulary mismatch happens.
	
	!!! tip "In the case of example, the 2nd line specifies that any CU instance of MuonCuType will move to the state READY whenever any state change in any of the CU's children results in all of those children now being in the state READY." 
		* The defined digital I/O DU has the states 'ON' and 'OFF'. 
		* State READY was never mentioned for 'Switch' DU!
		
	!!! warning "Such vocabulary mismatch between the states defined for a child and the states understood by a parent is a very common mistake that users make when setting up an FSM." 
		
	!!! help "Solution is to introduce the parent CU to the fact that its children DUs can adopt the states 'ON' or 'OFF'."

5.	Double click on the '`when all children in state DEFINED_STATE, move to DEFINED_STATE`' line.
	
	!!! info "In the case of the example, `when ($ALL$FwCHILDREN in_state READY) move_to READY` line."
	
6.	Fill in the table in the **"instr_when"** panel, introducing the parent CU to the fact that its children DUs may have a different states and what the parent should then do about it.

	![Placeholder](../assets/images/devguide/FSM/refine_instr_state.PNG){ loading=lazy : style="width:633px" } 
	
	!!! info "In the case of the example, introducing the parent CU to the fact that its children DUs may have a state 'ON' and what the parent should then do about it." 

7.	Note that the code section reflects the meaning of the choices defined using the drop down lists by autogenerating from selections.
	
	!!! info "In the case of the example, the updated line should be `when ($ALL$FwDioSwitchDuType in_state ON) move_to READY`."
	
	!!! warning "If the code line did not generate automatically with the selection, edit it manually."
	
8.	Click **"OK"** and close panels.

	!!! check "If changes were made successfully, the new definitions will be visible in the "SMI_Object_States" panel." 

9. 	Shift into the **"Operation Mode"**, click on the **"Stop all"** button and then the **"Start / Restart All"** button.

10.	Select the refined CU and right click on it and select the option **"View"**.

11.	Press the padlock alongside the relevant CU and take control.

!!! failure "If tried to press MuonCu 'CONFIGURE' (under the 'NOT_READY' button) in the example, nothing happens. Why?"
	The FSM has sent a message saying that the CONFIGURE action was propagated down to the MuonSubSystem DU but it does not know how to deal with this action when it is currently in state 'OFF'.
		
!!! tip "The parent (MuonCu) should be introduced to send an 'ON' action instead of a 'CONFIGURE' action to any children of type 'FwDiSwitchDuType'."	

### **Refine the Parent Vocabulary of Childrens’ Actions**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.
	
2.	Click on **"Create/Configure FSM Object Types"** button.

3.	In the **"FSM Types"** panel, double click on the CU Type that needs to be refined. 

4.	Click on the **"Type Overview"** button at the bottom of the **"SMI_Object_States"** panel of the relevant CU type. 

5.	Consider the **"view_type"** panel.
	
	![Placeholder](../assets/images/devguide/FSM/type_overview.PNG){ loading=lazy : style="width:653px" } 
	
	!!! info "The panel "view_type" present information on," 
		* Possible states for objects of the selected CU type.
		* Conditions in which ("when") the objects should move from one state to another.
		* Actions (commands) that objects of this type know about, when they are in each of their possible states. 
		
6.	Tick the box **"Show Action Code"** to expand the panel presenting instruction to objects in response to known actions (commands).
	
	![Placeholder](../assets/images/devguide/FSM/type_overview_action_code.PNG){ loading=lazy : style="width:653px" } 
	
	!!! info "In the case of the example, when the CU is in state NOT_READY and is commanded to 'CONFIGURE' it does not act." 
		Refine to send a 'SWITCH_ON' command to all of its children by manually editing the code.

7.	Close the **"view_type"** panel and double click on the action entry of the appropriate state in the **"SMI_Object_States"** panel.
	
	!!! warning "This step is done because it is not possible to actually edit the code in the "view_type" panel."
	
	!!! info "In the case of the example, double click on the 'CONFIGURE' action of the 'NOT_READY' state."

8.	In the **"action_editor"** popup panel, proceed to edit the script as required.
	
	![Placeholder](../assets/images/devguide/FSM/type_overview_action_editor.PNG){ loading=lazy : style="width:677px" } 
	
9.	Click **"OK"** when done and close the panel.

--8<-- "docs/includes/abbreviations.md"

