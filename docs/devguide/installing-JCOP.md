# **Installing JCOP Framework for WinCC OA** 

## **Installing CERN JCOP Framework**

1.	Visit https://jcop.web.cern.ch/jcop-framework. 

2.	Download both **"Framework"** and **"Installation Tool"** developed by CERN.

3.	Download **"JCOP Framework Components"** developed by CERN.

## **Installing CMS Specific JCOP Framework**

1.	Access the storage area available for users via personal credentials in the CMS specific environment i.e. <span style="color:blue">‘dfs(\\cern.ch)’ drive</span>.

2.	Open the **"FrameworkComponentSources"** directory (<span style="color:blue">Projects / CMSDCS / DCSRepository</span>) and download the latest version of JCOP installation tool and the production framework. 

	!!! info "Current DCS is developed on version 8.4.1 hence below tools and framework are recommended for installation purposes."
		*	**fw installation-8.4.1			: Installation tool**
	
		*	**jcop-framework-8.4.1 			: Framework** 
	
		*	**FrameworkInstallation316_841 	: CMS specific framework**

3.	Stop the current project and overwrite the content in the project directory with the content of the installation tool **"fwinstallation-8.4.1"**.

4.	Start the current project.

## **Checking Installation Status**

!!! check "If successfully installed, users might observe a new tab **'JCOP Framework'** in the menu bar."
	![Placeholder](../assets/images/devguide/JCOPfw/JCOP_tab.png){ loading=lazy : style="width:748px" } 


!!! failure "In case the ‘JCOP Framework’ is not visible in the Menu bar, follow the below steps."
	1. 	Click on the **"Vision"** button in the main toolbar.
		
		![Placeholder](../assets/images/devguide/JCOPfw/vision_icon.PNG){ loading=lazy : style="width:509px" } 

	2. Select **"fwinstallation"** folder in the **"Panel"** directory of the project.
	
	3. Select **"fwinstallation.pnl"** as the filename and run the file.

--8<-- "docs/includes/abbreviations.md"
