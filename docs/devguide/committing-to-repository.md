# **Managing Documentation Repository**

Once the author is satified with documents built on the DCS in a local level, the said documents should be uploaded to a repository such that it would manage the documentation website. 

## **Repositories Associated with GEM Documentation**

### **GitHub**

![Placeholder](../assets/images/devguide/Documentation/github_icon.png){ loading=lazy : style="width:50px" , align=left} 

The document repository for GEM DCS was previously maintained in GitHub and then shifted to GitLab. Even in that case GitHub is indirectly employed with current documentation process.

### **GitLab**

![Placeholder](../assets/images/devguide/Documentation/gitlab_icon.png){ loading=lazy : style="width:70px" , align=left} 

The document repository maintained by the team of developers for GEM DCS is at https://gitlab.cern.ch/cms-gem-dcs-project/gem-dcs-documentation.

## **Connecting with GitLab Repository via GitHub**
	
There are multiple methods to establish this connection. The presented method in this particular section is one of the easiest and straightforward methods of eastablishing the connection.

1.	Download the GitHub desktop application and log into it using GitHub credentials.

2.	Go to the GitLab repository, click on the **"Clone"** button and copy the http URL for the repository.
	
	![Placeholder](../assets/images/devguide/Documentation/repo_lab_URL.PNG){ loading=lazy : style="width:348px"} 

2.	Move to the **'Current Repository'** section, click on the **"Add"** button and select the **"Clone Repository"** option.
	
	![Placeholder](../assets/images/devguide/Documentation/repo_clone.PNG){ loading=lazy : style="width:404px"} 
	
3.	Click on the **"URL" tab** in the popup panel, provide the relevant URL of the GitLab repository and click on the **"Clone"** button.

	![Placeholder](../assets/images/devguide/Documentation/repo_URL.PNG){ loading=lazy : style="width:700px"} 

4.	Once cloned the current repository in GitLab will be fetched (downloaded) to the application and the user can make changes in the files inside fetched documents. 
	
	![Placeholder](../assets/images/devguide/Documentation/doc_fetched.PNG){ loading=lazy : style="width:700px"} 
	
## **Managing Repository Settings**

Before committing to the GitLab repository the user must ensure that settings are accurately configured. If not follow the steps below to manage settings.

1.	Click on the **'Repository'** tab in the menu bar and select **"Repository Settings"** option.
	
	![Placeholder](../assets/images/devguide/Documentation/repository_settings.png){ loading=lazy : style="width:369px"} 

2.	Check the **"Remote"** repository settings option and ensure the primary repository is included correctly.

	![Placeholder](../assets/images/devguide/Documentation/settings_remote.PNG){ loading=lazy : style="width:700px"} 

3.	Move to the **"Git Config"** repository option, select global configuration and include CERN credentials.
	
	![Placeholder](../assets/images/devguide/Documentation/settings_config.PNG){ loading=lazy : style="width:700px"} 

## **Committing to a Repository**

Once the connection is established and repository settings are managed accordingly, the user can commit the changes to the repository via the GitHub desktop application.

1.	If the user did changes (i.e. added files or images, edited files, etc.) then such changes can be viewed in the **'Changes'** section. 	

	![Placeholder](../assets/images/devguide/Documentation/doc_updates.PNG){ loading=lazy : style="width:700px"} 
	
	??? info "Color Code"
		![Placeholder](../assets/images/devguide/Documentation/color_code.PNG){ loading=lazy : style="width:370px"}
	
2.	Once the changes are ready to be commited, user must type a comment in the **"Summary (required)" (A)** section, include a descirption if preferred and click on the **"Commit to master"** button **(B)**.

	![Placeholder](../assets/images/devguide/Documentation/doc_comment.PNG){ loading=lazy : style="width:700px"} 
	
3.	It will take a moment to commit and once done, the user will see a confirming notification below the button which by the time would be disabled. Click the **"Push Origin"** button on the screen to push (upload) the changes to the repository.

	![Placeholder](../assets/images/devguide/Documentation/doc_push.PNG){ loading=lazy : style="width:700px"} 
	
4.	Within few seconds the pushed documents will be updated in the GitLab repository with a time stamp.

--8<-- "docs/includes/abbreviations.md"

