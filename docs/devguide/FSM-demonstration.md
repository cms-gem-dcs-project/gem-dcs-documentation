# **Designing an FSM Structure**

Instructions for designing an FSM are stated step by step and the figures presented within the content are based on an FSM built according to the structure below. 

![Placeholder](../assets/images/devguide/FSM/FSM_example.PNG){ loading=lazy : style="width:738px" } 

## **Prerequisites to Design an FSM**

1.	Project must have the JCOP Framework installed.
	
	!!! warning "The FwCore component is expected."
	
	!!! tip "No need to have the fwDim component installed to run the FSM."

2.	The FSM makes use of DIM communications and needs access to a **"DIM Name Server"** (<span style="color:blue">dns.exe</span>) somewhere in the universe. 

	!!! info "Running the server using the .exe file suffice for the creation." 
		* Exists fwinstallation directory of the project <span style="color:blue">C: / Projects / fwComponentsProject / bin / dns.exe</span>.
		
		* Select and run the application as an admin by right clicking on it and selecting **"Run as an Administrator"**.
		
			![Placeholder](../assets/images/devguide/FSM/DNS_admin.PNG){ loading=lazy : style="width:510px" } 

		* Run the dns.exe file as an administrator in the working universe and keep it running until the end of designing process.
		
			![Placeholder](../assets/images/devguide/FSM/DNS.PNG){ loading=lazy : style="width:731px" } 

3.	The fwFsmSrvr task must definitely be running.

	!!! help "Check the WinCC OA console to ensure that the fwFsmSrvr task is running."
		![Placeholder](../assets/images/devguide/FSM/server_PA.PNG){ loading=lazy : style="width:688px" } 

## **Setting the DIM Name Server (DNS) Node**

!!! warning "To setup a WinCC OA project to run the FSM users have to run a DIM Name Server (DNS) locally, refer the "Prerequisites to Design an FSM" section."

!!! danger "The host name of the currently running DNS is required in order to define the host of the DNS in the project." 

1.	Set the **"Device Editor and Navigator"** to the **"Operation Mode" (A)** and move to the **"FSM"** tab **(B)**. 
	
	![Placeholder](../assets/images/devguide/FSM/FSM_operation.PNG){ loading=lazy : style="width:283px" } 

2.	To define the DNS host, click on the **"DIM_DNS_NODE Undefined"** button **(C)**. 

3.	Enter the name of the DNS identity in the popup panel and press the **"OK"** button.
	
	![Placeholder](../assets/images/devguide/FSM/DNS_port.PNG){ loading=lazy : style="width:422px" } 

4.	After a brief period of processing the description on the button would indicate the DIM_DNS_NODE associated with the project.

	![Placeholder](../assets/images/devguide/FSM/DNS_connected.PNG){ loading=lazy : style="width:281px" } 

## **Control Unit Configuration**

### **Creating a Control Unit**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode" (D)** and move to the **"FSM"** tab **(B)**.
	
	![Placeholder](../assets/images/devguide/FSM/FSM_config.PNG){ loading=lazy : style="width:283px" } 

2.	Click on the **"Create/Configure FSM Object Types"** button **(E)**.

3.	Select **"Create Object Type"** button from the **"Object Type Configuration"** popup panel. 
	
	![Placeholder](../assets/images/devguide/FSM/object_types.PNG){ loading=lazy : style="width:278px" } 

4.	Provide a meaningful name for the CU type, confirm and wait for the CU type to appear in the **'Logical Objects Types'** section.
	
	![Placeholder](../assets/images/devguide/FSM/logical_object.PNG){ loading=lazy : style="width:292px" } 
	
4.	Double click on the created CU type.

5.	Click on the **"Simple Config"** button of the **"States and Actions"** popup panel which is also referred to as the **"SMI_Object_States"** panel.

	![Placeholder](../assets/images/devguide/FSM/CU_SMI_states.PNG){ loading=lazy : style="width:547px" } 
	
6.	Observe the default states offered and if appropriate for the created CU, select required states **(I)**, define actions **(II)** as preferred and click the **"Add"** button **(III)** next to each of the selected options.
	
	![Placeholder](../assets/images/devguide/FSM/CU_simple_config.PNG){ loading=lazy : style="width:498px" } 

	!!! tip "The image illustrates the declaration of READY, NOT_READY and ERROR states. All 3 created CU types have same declarations which will be refined later accordingly"
	
	!!! info "It automatically declares the actions possible in the field "Declared Actions" once the "Add" button is clicked for selected states with defined actions."
	
7.	Click the radio button **(IV)** to define the initial state at startup of the FSM.

	!!! tip "The image indicates the selection of "NOT_READY" as the initial state."
	
8.	Click the **"OK"** button **(V)** for the CU configuration and confirm in the **"SMI_Object_States"** panel and close the **"Object Types Configuration"** panel.

	![Placeholder](../assets/images/devguide/FSM/CU_SMI_states_defined.PNG){ loading=lazy : style="width:548px" } 
	
	??? info "Click here for further information on the sections "Panel", "List of States", "List of Actions" and "When List" after successfully defining states and actions."
		![Placeholder](../assets/images/devguide/FSM/SMI_object_detail.png){ loading=lazy : style="width:516px" } 
	
	!!! tip "Successfully defined states and actions are visible in the "SMI_Object_States" panel. Refer the image with updated States, Actions and When lists."	
	
	!!! check "The defining process for a Control Unit object type with states and associated actions end at this point."

### **Creating a Control Unit (Root Node) Instance**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.
	
2.	Move to the hierarchy, right click and select **"Add"** option from the list of options.
	
	![Placeholder](../assets/images/devguide/FSM/root_node.PNG){ loading=lazy : style="width:305px" } 

3.	Select **"Add New Object"** option from the **"Add Node"** popup panel.
	
	![Placeholder](../assets/images/devguide/FSM/add_node.PNG){ loading=lazy : style="width:305px" } 

4.	Select the CU type created earlier, provide a meaningful name for the instance and press **"OK"**.
	
	![Placeholder](../assets/images/devguide/FSM/root_new_node.PNG){ loading=lazy : style="width:325px" } 
	
	!!! tip "In the case of the example, the root node is "DcsCu" of the "DcsCuType"."
	
5.	Click **"OK"** in the **"Add Node"** panel.
	
	![Placeholder](../assets/images/devguide/FSM/root_add.PNG){ loading=lazy : style="width:305px" } 
	
	!!! info "Wait until the node is set up. Once done it would appear in the heirarchy."
	
	!!! tip "Refer the image and observe the created root node "DcsCu" in the list of structures."
	
	![Placeholder](../assets/images/devguide/FSM/hierarchy_root.PNG){ loading=lazy : style="width:282px" } 

	!!! warning "If the system is large, check to see that the log file has gone quiet. This may not happen for some time after the progress popup window disappears."

### **Adding a Child Node to the Control Unit**

!!! danger "Users might require to insert subdetectors for some or all CU created of the DCS which could be done by adding a child node to each such CU."

!!! warning "The subdetector systems are likely to have been implemented by different groups hence definitions of children’s states and actions might differ from one to another." 

!!! help "In the case where they are different, user will have to employ distinct CU types to make the mapping into the states or vocabulary."

!!! info "In the case of the example, states and actions of Muon subsystem children might be different from the definitions of states and actions of Tracker subsystem children when mapping vocabulary to DcsCu."

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.

2.	Move to the created CU root node or node, right click, select **"Add.."** and the **"Objects"** option from the extended list.
	
	![Placeholder](../assets/images/devguide/FSM/child_nodes.PNG){ loading=lazy : style="width:283px" } 

3.	Select **"Add New Object"** from the **"Add Node"** panel. 
	
4. 	In the **"Add Object"** popup panel add a new node (CU object) of type created earlier, providing a meaningful name and press **"OK"**.
	
	![Placeholder](../assets/images/devguide/FSM/CU_child_node.PNG){ loading=lazy : style="width:325px" } 

	!!! tip "Refer the image presenting the creation of a child node "MuonCu" of the "MuonCuType" for the DcsCu root."
	
5.	Tick the **"as Control Unit"** box in the **"Add Node"** panel. 
	
	![Placeholder](../assets/images/devguide/FSM/CU_add_tick.PNG){ loading=lazy : style="width:305px" } 

	!!! tip "The image illustrates the created CU object "MuonCu" of the CU type "MuonCuType" and the type is confirmed by the tick in the "as Control Unit" box for this particular object."

6.	Click **"OK"** to create the CU child node. 

	!!! info "Wait while the CU is created and once done the child CU will appear in the hierarchy structure as a child node."
	
	!!! tip "Refer the image and observe the created child nodes for the "DcsCu" root node. There are 2 such child nodes,"
		* **"MuonCu"** of the CU type **"MuonCuType"**
		* **"TrackerCu"** of the CU type **"TrackerCuType"**
		* Both definitions are similar regarding states and actions which **will be refined** later.
	
	![Placeholder](../assets/images/devguide/FSM/hierarchy_child.PNG){ loading=lazy : style="width:282px" } 

	!!! warning "If it appears as a sibling of the CU, then delete it using the right click and try again."
	
7.	Set the **"Device Editor and Navigator"** to the **"Operation Mode"**.

8.	Check if the DNS node is processing and click **"Start/Restart All"** button and wait whilst all of the FSM domain starts up.
	
	![Placeholder](../assets/images/devguide/FSM/CU_child_restart.PNG){ loading=lazy : style="width:283px" } 
	
	!!! check "If the DIM Name Server is running, the name appears next to "DIM_DNS_NODE :" on the button description."
	
	!!! failure "If the DIM Name Server is not running in the universe of the current project, user will recieve an error notification."

### **View the Running FSM**

1.	Select the CU, right click and select the **"View"** option.
	
	![Placeholder](../assets/images/devguide/FSM/FSM_view.PNG){ loading=lazy : style="width:283px" } 

	!!! tip "The padlocks remain unlocked implying availability for someone to take control. Not even the creator has control yet."

2.	Click on the CU system padlock and take control by appointing a privileged user account.
	
	!!! info "The colours representing the states of the subsystems sould be visible at this stage and would be in the appointed initial state."
	
	!!! help "Click on a blue state to get a list of possible actions. If the user selected a defined action, it would be propagated to child subsystem(s) and shift states accordingly."
	
### **Partitioning the FSM**

!!! danger "Partitioning faciliates the excluding of a child node from the user jurisdiction or control."

1.	Press the **padlock icon** next to the node that needs to be excluded. 

2.	Click the **"Exclude"** option from the popup panel.

	!!! check "The panel would close automatically and the partitioned state box would turn grey."
	
	!!! warning "State changes will no longer be taken into account when computing the state of the system over which the user do have control."
	
	!!! danger "The padlock would be ringed in orange to warn the user that some child subsystem below the CU is currently excluded."
	
## **Device Unit Configuration**

### **Creating a Device Unit**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.

2.	Select **"Add Device Type"** button from the **"Object Types Configuration"** panel.
 
	![Placeholder](../assets/images/devguide/FSM/device_types.PNG){ loading=lazy : style="width:279px" } 

3.	Select the required DU type from the **"Framework Device Types"** section in the **"Add Device Type"** panel and press **"OK"**.
	
	![Placeholder](../assets/images/devguide/FSM/new_device.PNG){ loading=lazy : style="width:292px" } 

4.	Provide a meaningful name for the new device type. 

	![Placeholder](../assets/images/devguide/FSM/add_new_device.PNG){ loading=lazy : style="width:323px" } 
	
	!!! info "After a brief pause, the unit would appear in the list of **"Framework Device Types"** section with the prefix of the selected DU type."
	
	!!! tip "In the case of the example, 2 such device types are created."
		* **"SwitchDuType"** that appears prefixed with **"FwDio"**
		* **"TemperatureDuType"** that appears prefixed with **"FwAi"**
	
5.	Double click on the created DU type.

6.	Click on the **"Simple Config"** button of the **"SMI_Object_States"** popup panel.
	
	![Placeholder](../assets/images/devguide/FSM/DU_SMI_states.PNG){ loading=lazy : style="width:547px" } 
	
7.	Observe the states offered and select the appropriate states **(I)** for the created DU according to requirements, define actions **(II)** as preferred and click the **"Add"** button **(III)** next to each of the selected options.
	
	![Placeholder](../assets/images/devguide/FSM/DU_simple_config.PNG){ loading=lazy : style="width:473px" } 

	!!! info "It automatically declares the actions possible in the field "Declared Actions" once the "Add" button is clicked for selected states with defined actions."
	
	!!! tip "The image illustrates the editing of ON and OFF states and actions. The system will automatically select possible actions."

8.	Select **"State depends on DP item" (IV)** and choose **"inValue"** from the list of options.

9.	Similarly select **"Actions act on DP Item" (V)** and choose **"outValue"** from the list of options. 

	!!! tip "These "inValue" and "outValue" are obtained from the datapoint element names within a specific type."
	
10.	Click the **"OK"** button **(VI)** to confirm the DU configuration.

	![Placeholder](../assets/images/devguide/FSM/DU_SMI_states_defined.PNG){ loading=lazy : style="width:570px" } 
	
	??? info "Click here for further information on the sections "Panel", "List of States", "List of Actions" and "Configure Device" after successfully defining states and actions."
		![Placeholder](../assets/images/devguide/FSM/SMI_device_detail.png){ loading=lazy : style="width:531px" } 
	
	!!! tip "Successfully defined states and actions would be visible in the "SMI_Object_States" panel. Refer the image with updated lists of states and actions."	

	!!! check "The defining process of an object type with states and associated actions end at this point but hardware should be linked for the completion of the DU."
	
### **Linking to Hardware**

!!! danger "Linking in this instruction series is according to the units, states and actions defined in previous sections. Depending on requirements users might need to select different options"

1.	Click on the **"Configure Device"** button and select **"Configure Device States" (VII)** option from the list in the **"SMI_Object_States"** panel.

	![Placeholder](../assets/images/devguide/FSM/DU_SMI_config.PNG){ loading=lazy : style="width:570px" } 

2.	Fill in the fields to according to the requirements of the FSM **(IX)**.
	
	![Placeholder](../assets/images/devguide/FSM/config_device_states.PNG){ loading=lazy : style="width:577px" } 

	!!! tip "In this specific case as the figure illustrates, definition (invalue ==1) means ON."

3.	Press **"Generate Script (from screen)"** button **(X)**.

4.	Click on the **"Edit Script"** button **(XI)**, read the code and ensure that the code makes sense. 

	!!! warning "This apparently unnecessary step is actually a good habit to get into especially if the user is a developer."

5.	Press **"Close"** button **(XI)** and exit.

	!!! check "The process of defining how the future instances of this type should determine their specific states is done."

### **Configuring Actions**

!!! danger "All actions in a DCS FSM are carried out by responding to commands hence defining instructions for instances to respond with regard to commands is important."

1.	Go to the **"SMI_Object_States"** panel.

2.	Select **"Configure Device"** and select **"Configure Device Actions" (VIII)** from the dropdown menu.
	
	![Placeholder](../assets/images/devguide/FSM/config_device_actions.PNG){ loading=lazy : style="width:536px" } 

3.	Arrange FSM actions according to the requirements of the DCS **(XIII)**.
	
	!!! tip " In this specific case, action SWITCH_OFF is set to the DPE 'outValue' to 0. Correspondingly, the FSM action SWITCH_ON is set to the DPE value 1."

4.	Click **"Generate Script (from screen)"** button **(XIV)** and close the panel.

5.	Press the **"Close"** button **(XV)** on the **"SMI_Object_States"** panel and close the **"Object Types Configuration"** panel.

### **Creating a Device Unit Instance**

1.	Set the **"Device Editor and Navigator"** to the **"Configuration Mode"** and move to the **"FSM"** tab.

2.	Right click on the DU type, select **"Add"** and choose **"Devices"** from the extended menu.
	
	![Placeholder](../assets/images/devguide/FSM/device_nodes.PNG){ loading=lazy : style="width:283px" } 

3.	Select **"Add Devices from Hardware View"** option from the popup panel. 
	
	![Placeholder](../assets/images/devguide/FSM/add_device_node.PNG){ loading=lazy : style="width:305px" } 

4.	Select DU type created earlier from the **"Add Devices"** panel.

5.	Select required items and move them into the right hand side using **">"** or if all items are required using **">>"**.
	
	![Placeholder](../assets/images/devguide/FSM/device_items.PNG){ loading=lazy : style="width:600px" } 

6.	Click **"OK"** in the **"Add Node"** panel.

	!!! check "Wait while the nodes in the FSM tree are set up once done it would appear in the heirarchy."

7.	<span style="color:red">Do not</span> tick the **"as Control Unit"** box in the **"Add Node"** panel. 
	
	!!! warning "In the "Add Node" panel, do NOT tick "as Control Unit" box because this is a device unit!"

	![Placeholder](../assets/images/devguide/FSM/DU_no_tick.PNG){ loading=lazy : style="width:305px" } 

	!!! tip "The image illustrates the created DU object "MuonSubSystemPower" of the DU type "FwSwitchDuType"."

8.	Click **"OK"** to create the hardware node.

	!!! info "Wait while the DU is created and once done the child DU will appear in the hierarchy structure as a hardware child node."
	
	!!! tip "Refer the image and observe the created hardware nodes for the child nodes of the "DcsCu" root node. There are 2 such child nodes and within them 1 hardware node each,"
		* **"MuonCu" with the hardware node "MuonSubSystemPower"**
		* **"TrackerCu" with the hardware node "TrackerTemperature"**
		
	![Placeholder](../assets/images/devguide/FSM/hierarchy_hardware.PNG){ loading=lazy : style="width:283px" } 

	!!! warning "If it appears as a sibling of the CU, then delete it using the right click and try again."
			
9.	Shift to the **"Operation Mode"**.

10.	Check if the DIM Name Server is processing and press **"Stop All"** and do not skip this step. Then click **"Start/Restart All"** button.
	
	![Placeholder](../assets/images/devguide/FSM/DU_node_restart.PNG){ loading=lazy : style="width:283px" } 
	
	!!! check "If the DIM Name Server is running, the name appears next to "DIM_DNS_NODE :" on the button description."

	!!! failure "As DU definitions are edited, omitting this step before going on to the next step "Restart All" will cause an internal mismatch."
	
--8<-- "docs/includes/abbreviations.md"
