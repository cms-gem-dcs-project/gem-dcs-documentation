# **Configuration Database Tool of JCOP Framework** 

## **Manually Setting Up the Database with Config DB**

1.	Check if the **"CMSfwInstallUtils"** component is installed in WinCC OA. If not, refer to the "Getting Started with JCOP Framework" and install the component.

2.	Click on the **"JCOP Framework"** tab from the menu bar and select **"Device Editor Navigator"** option from the dropdown menu.
	
	![Placeholder](../assets/images/devguide/JCOPfw/JCOP_DEN.PNG){ loading=lazy : style="width:426px" } 

3.	Select the **"CMSfwInstallUtils" (B)** directory in the **"Hardware"** tab in the **"Operation Mode" (A)**.
	
	![Placeholder](../assets/images/devguide/JCOPfw/installUtils_DB.PNG){ loading=lazy : style="width:283px" } 

4.	Ensure the database connection is configured correctly. The database connection status **(C)** is shown on the popup panel.
	
	![Placeholder](../assets/images/devguide/JCOPfw/installUtils_DB_filter.PNG){ loading=lazy : style="width:552px" } 

5.	Click on the **"New Filter" (D)** button from the **"Select Filter"** area. A new window will popup to create or edit a filter. 

6.	Complete following information in the popup window.
	
	![Placeholder](../assets/images/devguide/JCOPfw/installUtils_DB_filter_pop.PNG){ loading=lazy : style="width:500px" } 

	- [x] **(I)** : Type a **"Filter Name"** in the **"Filter Information"** area.
	- [x] **(II)** : Type a **"Database Config Name"** in the **"Filter Information"** area.
	- [x] **(III)** : Click the **"+"** sign next to the **"Database Config Name"** typing space. 
	- [x] **(IV)** : Select the required DP type from the dropdown list.
	- [x] Add the selected DP type by clicking on **(V)**. 
	- [x] **(VI)** Click on the **"Save Filter"** button.

7.	Move back to the **"CMSfwInstallUtils"** panel to select the created filter from the list of filters from the **"Select Filter"** area.

8.	Click on the **"View Device List"** button **(E)** and the DP list will appear in the table in the **"Filter List"** area.

9.	Click on the **"Save to Conf DB"** button **(F)** to save the configuration.

10.	If the user needs to check the DP status click on the **"Perform DB Checks" (G)** which would display the DP status for all DPs in a table.

--8<-- "docs/includes/abbreviations.md"
