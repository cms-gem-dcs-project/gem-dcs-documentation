# **Creating a Runtime Environment with DB Editor Navigator Tool**

!!! danger "The **"DB Editor Navigator"** tool is used to store the overall configuration of the Detector Control System in the central database."

## **Opening a Terminal Server**

The user might have to use one of the following methods to access the tool via a terminal server depending on the Operating System and the location before finding the option DB Editor and Navigator.

=== "Windows Operating Systems"
	
	??? info "From Inside CERN" 
		![Placeholder](../assets/images/devguide/dbNav/set_terminal.PNG){ loading=lazy : style="width:407px" , align=left} 

		- [x] Open Remote Desktop Connection app.
		- [x] Enter `cerntscms.cern.ch` for the computer name and `CERN\<cern_user_name>` as the user name.
		- [x] Click connect and provide the CERN password.
	
    ??? info "From Outside CERN" 
		- [x] Open the terminal and login to personal lxplus account. 
		- [x] Enter `ssh -Y <cern_user_name>@lxplus6.cern.ch`.
		- [x] Enter `rdesktop -g 100% -a16 -u <cern_user_name> -d CERN cerntscms.cern.ch`.

=== "Linux Operating Systems"
	
	??? info "From Inside CERN" 
	
		- [x] Open the terminal.
		- [x] Enter `rdesktop -g 100% -a16 -u <cern_user_name> -d CERN cerntscms.cern.ch` 
	
    ??? info "From Outside CERN" 
		- [x] Open the terminal and login to personal lxplus account. 
		- [x] Enter `ssh -Y <cern_user_name>@lxplus6.cern.ch`.
		- [x] Enter `rdesktop -g 100% -a16 -u <cern_user_name> -d CERN cerntscms.cern.ch`.

Select **"DB Editor and Navigator Dev"** from the CMS Terminal Server.

![Placeholder](../assets/images/devguide/dbNav/terminal.PNG){ loading=lazy : style="width:522px"} 

The **"DB Editor and Navigator"** tool will appear after entering the necessary commands above.

![Placeholder](../assets/images/devguide/dbNav/dbNav_tool.PNG){ loading=lazy : style="width:652px"} 

## **Registering a Host Computer**

Before starting actions on the DB Editor and Navigator tool, the user should register a host computer.

1.	Click on the node **"Computers"**.
	
2.	Click on the **"Add Host"** button appears under the table popped up on the right hand side.

	![Placeholder](../assets/images/devguide/dbNav/add_host.PNG){ loading=lazy : style="width:674px"} 

3. 	Enter the hostname of the machine that needs registering (eg. `hostMachine.cern.ch`). 
	
	![Placeholder](../assets/images/devguide/dbNav/host_computer.PNG){ loading=lazy : style="width:459px"} 

4.	Click **"OK"** and a popup will inform the outcome of the procedure.

	![Placeholder](../assets/images/devguide/dbNav/host_computer_confirmed.PNG){ loading=lazy : style="width:453px"} 

5.	Close the panel and move to the tree widget. It will automatically refresh and the newly added host will be visible in the huerarchy.

	![Placeholder](../assets/images/devguide/dbNav/host_computer_tree.PNG){ loading=lazy : style="width:455px"} 

	!!! failure "In the case automatic refresh does not occur follow the procedure below."
		If the tree widget is not refreshed, select **'System Hierarchy'** from **"Switch to Mode"** dropdown menu in bottom left corner and change back to **'Projects Setup'**.
		
		![Placeholder](../assets/images/devguide/dbNav/switch_to_mode.PNG){ loading=lazy : style="width:333px"} 

## **Registering a Project**

1.	Click on a host node on the tree.

2.	Click on the **"Add Project"** button.
	
	![Placeholder](../assets/images/devguide/dbNav/add_project.png){ loading=lazy : style="width:673px"} 

3.	Click on **"Wizard"** button in the new panel.
	
	![Placeholder](../assets/images/devguide/dbNav/add_project_panel.PNG){ loading=lazy : style="width:609px"} 

4.	Click **"OK"** without changing anything in the new popup window and fields of will automatically fill in with predefined values.
	
	![Placeholder](../assets/images/devguide/dbNav/add_project_wizard.PNG){ loading=lazy : style="width:607px"} 

5. Once confirmed the creation of the project, the user can edit the project according to requirements.

	- [x] Change the project name as preferred.
	- [x] The project directory will automatically update with the project name.
	- [x] Provide a proper name in the that had not been used earlier **'PVSS System Info'** section.
	- [x] The button marked as **"Show Used"** displays all the (used) system names and a dropdown marked as **"Available"** shows the smallest available number that can be used as a system number.
	- [x] Click **"Apply"** and **"Ok"** and close the panel.

5.	The tree widget will refresh, showing the newly added project under the host.
	
	![Placeholder](../assets/images/devguide/dbNav/added_project_tree.PNG){ loading=lazy : style="width:603px"} 

## **Adding Project Paths**

1.	In the process of saving the new project user will recieve the below notification, which suggests the importance of defining a path.

	![Placeholder](../assets/images/devguide/dbNav/path_notification.PNG){ loading=lazy : style="width:335px"} 

2.	Select the relevant project and go to the **'Add Paths'** tab.  
	
	![Placeholder](../assets/images/devguide/dbNav/add_path_tab.PNG){ loading=lazy : style="width:880px"} 

3.	Click on the host machine name **(I)** in the table.
	
	![Placeholder](../assets/images/devguide/dbNav/add_path.PNG){ loading=lazy : style="width:902px"} 

4.	Type the directory in the field **(II)** provided.
	
	!!! info "Depending on the WinCC OA version the installation directory differes."
		* For WinCC 3.15 use <span style="color:blue">C:\DCS\DCSRepository\FrameworkInstallation315_841</span>
		* For WinCC 3.16 use <span style="color:blue">C:\DCS\DCSRepository\FrameworkInstallation316_841</span>

5.	Click on the **III** button and check if the path added to the list.

6.	Click on the **IV** button to confirm all actions.

7.	Click the **"Apply"** button to confirm and apply the path.

## **Targeting Components to a Project**

### **Create a Group**

1.	Click on the **"Computers"** node on the tree widget.

2.	Select the **'Edit Groups'** tab, which allows the creation, removal and modification of component groups. 
	
	![Placeholder](../assets/images/devguide/dbNav/component_edit_tab.PNG){ loading=lazy : style="width:888px"} 

3.	Click on **"Create New"** button to create a new group.

	![Placeholder](../assets/images/devguide/dbNav/add_component_group.PNG){ loading=lazy : style="width:881px"} 

4.	Provide a name in the **'New component group name'** field.

5.	Select proper **'Access control domain'** (i.e `CMS_GEM`) from the dropdown list available.

6.	Click on the **"Apply"** button and then the **"Ok"** button.

### **Adding Components to a Group**

1.	Select the created group **(I)** and click on the **"Modify Group"** button **(II)** to add components to the group.
	
	![Placeholder](../assets/images/devguide/dbNav/component_modify.PNG){ loading=lazy : style="width:880px"} 

2.	Click on the folder icon in the popup panel and direct the path to the destination of the component.
	
	![Placeholder](../assets/images/devguide/dbNav/component_modify_popup.PNG){ loading=lazy : style="width:873px"} 

	!!! tip "The user should know the components employed in the DCS project. The component list could be found via the postinstall xml file of the project."
	
	!!! info "Usually all the production components are located under <span style="color:blue">C:/DCS/DCSRepository/ComponentSources</span>." 

3.	Select the required components and add them by clicking on **'Add to Group'** column, which shows the text **'Add' (I)** when clicked and clicking the **"Add"** button **(II)**.
	
	![Placeholder](../assets/images/devguide/dbNav/component_add_delete.PNG){ loading=lazy : style="width:866px"} 
	
	??? info "In cases of dependencies components require other components to perform their tasks, which should also be added. In such cases a popup dialog box will inform the user of the fact."
		![Placeholder](../assets/images/devguide/dbNav/component_dependencies.PNG){ loading=lazy : style="width:720px"} 
		
4.	Similarly if a component needs to be removed, click on the **"Delete"** button **(IV)** after selecting the components to be removed in the **'Delete' (III)** column."
	
5.	Click **"Ok"** when all required components are added.

	![Placeholder](../assets/images/devguide/dbNav/component_save.PNG){ loading=lazy : style="width:853px"} 

6. 	Select the project from the tree widget and go to the **'Components'** tab.

	![Placeholder](../assets/images/devguide/dbNav/component_tab.PNG){ loading=lazy : style="width:881px"} 

7.	Move to the left side of the panel, which presents all the available groups registered including ones registered by other users. 
	
	![Placeholder](../assets/images/devguide/dbNav/component_save.PNG){ loading=lazy : style="width:853px"} 

8.	Use the arrows **">"** and **"<"** between the 2 tables to add or remove groups. 
	
	![Placeholder](../assets/images/devguide/dbNav/component_group.PNG){ loading=lazy : style="width:763px"} 

	!!! warning "If someone else’s group was selected, they may change the contents of their group at any given time and such components will be untargeted from the project."
	
9.	Click on the **"Apply"** button.

Once the above procedure is done the tool based part of the **"DB Editor and Navigator"** is complete and a single click on the project from the tree widget would be like the following image.
	
![Placeholder](../assets/images/devguide/dbNav/dbNav_tool_done.PNG){ loading=lazy : style="width:761px"} 

## **Set the Developing Environment Daatbase**

Refer the configuration of database section.

## **Creating a Registered Project**
 
In order to create a project using the information stored in the database, follow the instructions below.

1.	Open a command line with admin privilages) and execute the command,
	
	!!! note ""
		If the system is WinCC OA 3.15 :
		<span style="color:blue">`G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation\CreateServiceProject.bat`</span>
	
	!!! note ""
		If the system is WinCC OA 3.16 :
		<span style="color:blue">`G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation_3.16\CreateServiceProject.bat`</span>

2.	A list of the projects registered in the database for this host will appear under **"Available projects for installation on host HOST_NAME:"**. 

	!!! Warning "If the development machine has both WinCC OA 3.15 and 3.16 installed and if a 3.15 project is required, then running above command will result in an error." 
		* Copy all the files inside <span style="color:blue">G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation</span> to the local folder in the user device.
		* Edit the bat file **'SetBaseVariables.bat'** and remove the part that searches for 3.16 version. 
		* Save and run **'CreateServiceProject.bat'** file exist in the local folder. 
		
3.	Type the number of the project to create infront of **"Enter project number"**.

4.	Press **"Y"** to confirm the process **"Do you want to proceed with the installation? [Y,N]?"**.

5.	The installation process of the project will continue and at some point user will be prompted for requesting a license which confirms the successful creation of the project.
 
6.	Close that license page and the command line will appear as in the following image.

7.	Open the WinCC OA console and the newly created project will appear in the list of projects.

8.	Start the project. Note that, this will not open the GEDI.

	!!! warning "WinCC OA 3.16 version might give an error message in a dialog box in console when the newly created project is selected. 
	
	!!! tip "To resolve this problem, create a folder 'fwComponents_<project name>' inside the directry <span style="color:blue">C:\DCS\WCCOAProjects</span> and then run the command in step 1."

9.	Open the project shortcut file on the desktop and it will open the GEDI related to the created project.

10.	When started, the project automatically connects to the database and starts installing all targeted components.
 
11.	At this point the project is up and running and if logged into **'DB Editor Navigator'** tool, the view of the interface will be as in the following image.

### **Locally Installing New Components to Projects**
 
1.	Go to the terminal server again and open the **'DB Editor Navigator'** tool.

2.	Observe that the **'management mode'** is currently set to **"Central"**.

3.	Change that into **"Local"** by pressing on the button **"Set to Centrak"**.

4.	Move back to the development machine and open a command line with admin privilages to execute the following command.

	!!! note ""
		If the system is WinCC OA 3.15 :
		<span style="color:blue">`G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation\CMS_installNewComps.bat`</span>

	!!! note ""
		If the system is WinCC OA 3.15 :
		<span style="color:blue">`G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation_3.16\CMS_installNewComps.bat`</span>
	
	!!! Warning "If the development machine has both WinCC OA 3.15 and 3.16 installed and if a 3.15 project is required, then running above command will result in an error." 
		* Copy all the files inside <span style="color:blue">G:\Users\c\cmsdcs\Public\DCSFiles\CMF\ProjectInstallation</span> to the local folder in the user device.
		* Edit the bat file **'SetBaseVariables.bat'** and remove the part that searches for 3.16 version. 
		* Save and run **'CMS_installNewComps.bat'** file exist in the local folder. 
		* Project must be running, when executing this command.
		
5.	Select and type the number of the project requires configuring in front of **'Select project number for HOST_NAME:'**.

7.	The project is currently running in the Console but GEDI is not opened yet. Go to the desktop and open the shortcut file having WinCC OA icon with the file name containing the project name and the port number, to open the GEDI. 

8.	In the UI, open the **'Installation'** tool by hovering over the tab **'JCOP Framework'** and follow the instructions in component installation section.


 


