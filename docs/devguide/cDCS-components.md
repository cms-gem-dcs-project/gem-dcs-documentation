# **Adding DCS Components to the cDCS**

!!! danger "cDCS = central Detector Control System"

DCS components could successfully added to the central DCS if the user followed instructions given below for the process.

1.	Create the GIT repository in a production branch. 

	!!! tip "The 'Component Handler' only considers the production branch before creating a new component." 
	
	!!! warning "The same name must be used in both the 'Component Handler' and in the GIT repository."

2.	Visit the repository for CERN via https://cmsonline.cern.ch using CERN credentials.

3.	Select **"Component Handler" (III)** of **"DCS" (II)** for GEM detector from the left sidebar under **"Common" (I)**.
	
	![Placeholder](../assets/images/devguide/JCOPfw/component_handler_path.PNG){ loading=lazy : style="width:263px" } 
	
4.	Create the component in the 'Component Handler' by clicking on the **"Create Component >>"** button.
	
	![Placeholder](../assets/images/devguide/Special/cDCS_create.png){: style="width:613px"}
	
5.	Click on the **"Import Files from GIT >>"** button in the next destination.

	![Placeholder](../assets/images/devguide/Special/cDCS_import_GIT.png){: style="width:617px"}
	
6.	Provide the required version to be imported and select the revision and then click the **"OK"** button.

	![Placeholder](../assets/images/devguide/Special/cDCS_version.png){: style="width:619px"}
	
7.	Provide the component name accurately in field and click the **"OK"** button.

	![Placeholder](../assets/images/devguide/Special/cDCS_component.png){: style="width:616px"}
	
8.	Click on the **"Browse Components"** link **(A)** in the **'Dependent Components'** section to browse dependencies of the entered component.

	![Placeholder](../assets/images/devguide/Special/cDCS_dependencies.png){: style="width:619px"}
	
9.	If there exist dependent components, search via the search tab, tick the box next to search result if the result of the search is the requried component and click the **"OK"** button.

	![Placeholder](../assets/images/devguide/Special/cDCS_depend_search.png){: style="width:619px"}
	
10.	Repeat the process for all required dependencies and move to **'Set Active Component'** section, tick the **"Is Active"** box **(B)** and click the **"Apply"** button.

	!!! info "When a new version is created, it inherits dependencies and configurations from the highest version."
		**If the recreation of the same version number without losing the 'Component Handler' configuration, user should follow the instructions below.**
	
		* Create a new version X.Y.Z+1 importing the latest commit (this will inherit dependencies/configurations from X.Y.Z)
		* Delete the current version X.Y.Z
		* Create X.Y.Z again, importing the latest commit (this will inherit dependencies/configuration from X.Y.Z+1)
		* Delete X.Y.Z+1

11.	Contact cDCS to target the newly created component and once targeted, it will be installed immediately.

--8<-- "docs/includes/abbreviations.md"
