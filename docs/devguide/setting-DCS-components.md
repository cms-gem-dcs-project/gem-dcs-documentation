# **Setting Up GEM DCS Components** 

1.	Visit the git repository for CERN via https://cmsonline.cern.ch using CERN credentials.

2.	Select **"DCS" (II)** for GEM detector from the left sidebar under **"Common" (I)**.
	
	![Placeholder](../assets/images/devguide/JCOPfw/component_handler_path.PNG){ loading=lazy : style="width:263px" } 
	
3.	Select **"Component Handler" (III)** and wait for the popup panel to appear and select **"GEM"** from the popup panel.
	
	![Placeholder](../assets/images/devguide/JCOPfw/component_handler.PNG){ loading=lazy : style="width:599px" } 
	
	Refer the section **(A)** in the image.
	!!! info "Components the user should download from the **'Component Handler'** that are required for the GEM DCS are as follows." 
		*	**CMS_GEM_GE11_CAEN**
		
		*	**CMS_GEM_GE11_DetectorProtection**
		
		*	**CMS_GEM_GE11_FSM**
		
		*	**CMS_GEM_GE11_GAS**
		
		*	**CMS_GEM_GE11_UI**
	
	Refer the section **(B)** in the image.
	!!! help "If the user requires to move onto a previous directory for some reason, use the **'Go Back' icon (B)** in the image instead of the back icon on the web page."

4.	Download required components from the panel. Click on the name of the required directory and then click on the first downloadable ZIP folder **"(C)"** in the directory.
	
	![Placeholder](../assets/images/devguide/JCOPfw/component_handler_in.PNG){ loading=lazy : style="width:598px" } 
	
5.	Hover over the tab **"JCOP framework"** and click on the **"Installation"** option from the dropdown menu.

6.	Include the path to the project directory and wait until the relevant file is done being loaded.

	!!! tip "Some components require others as prerequisites for installation. Refer to the requirements mentioned in XML documents to install components in correct order."

7.	Click on the **"Install"** button and confirm the download destination as the project directory.
	
	!!! info "Advanced Options"
		![Placeholder](../assets/images/devguide/JCOPfw/component_handler_adv.PNG){ loading=lazy : style="width:392px" , align=left} 
		
		If expected destination is not the auto selected project directory, click on **"Advanced Options"** and include the path to the project directory as the destination.
	
		- [x] The selected destination directory should appear with a grayish highlight in the 'Installation Directory' **(I)** section.
	
		- [x] If it does not appear with a grayish highlight, click the icon **(II)**, select the path and click **"Add" (III)** button.

8.	Click **"Run Postinstall Scripts"** to finish the installation.
	
	![Placeholder](../assets/images/devguide/JCOPfw/fwpopup_restart.PNG){ loading=lazy : style="width:365px" } 

	
--8<-- "docs/includes/abbreviations.md"
