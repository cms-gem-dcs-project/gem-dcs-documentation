# **FSM Structure Overview**

The hierarchical controls part of the framework allows the definition and operation of hierarchies of objects behaving as Finite State Machines which allows the sequencing and automation of operations. 

!!! danger "FSM = Finite State Machine"

The mechanism adopted for modelling the structure of subdetectors, subsystems and hardware components in a consistent fashion is to use a hierarchical (tree like) structures composed of 2 types of nodes.
 
* <span style="color:DarkCyan">**Device Units (DU)**</span> which are capable of monitoring and controlling equipments to which they correspond. 

* <span style="color:DodgerBlue">**Control Units (CU)**</span> or <span style="color:RoyalBlue">**Logical Units (LU)**</span> which are considered to contain FSM capable of modelling and controlling the subtree below them. 

* Nodes of the tree are CUs and DUs always appear as leaves of the tree. 

Refer the example structure with definitions.

![Placeholder](../assets/images/devguide/FSM/example_FSM.PNG){ loading=lazy : style="width:671px" } 

## **Definitions**

### <span style="color:Crimson">**State Management Interface (SMI)**</span>

SMI is the FSM toolkit incorporated in the framework which allows the description of any subsystem as a collection of objects, each object behaving as a FSM. SMI defines 2 types of objects. 

* <span style="color:LightCoral">**Abstract Objects**</span> which implement logic bahaviour. When an action gets triggered they execute instructions. 
	
* <span style="color:Salmon">**Physical Objects**</span> which implement the interface to real devices. When they receive a command, should act on the device model and when the device’s data change, might have to change the state. 

* Both object types possess a list of allowed states and a list of allowed actions for each state. 

### <span style="color:DodgerBlue">**Control Unit (CU)**</span>  / <span style="color:DeepSkyBlue">**Logical Unit (LU)**</span>  

Instance of a <span style="color:RoyalBlue">**Logical Unit Type**</span> running as a <span style="color:MediumVioletRed">**SMI Domain**</span>. CUs are logical decision units and can take decisions to act on their children. 

!!! note ""
	<span style="color:RoyalBlue">**Logical Unit Type**</span> : Modelling of behavior for an abstract object.

!!! bug ""
	<span style="color:MediumVioletRed">**SMI Domain**</span> : A logically related group of objects (a subsystem) composed of one more objects of one or both types.

State transitions cause the evaluation of logical conditions and this mechanism can be used to propagate actions down the tree, to automate operations and to recover from error situations.

### <span style="color:SeaGreen">**Device Unit (DU)**</span>   

Instance of a <span style="color:DarkCyan">**Device Unit Type**</span> based on a <span style="color:LightSeaGreen">**Device**</span> and implement the interface with the lower level components (Hardware or Software).

!!! tip ""
	<span style="color:DarkCyan">**Device Unit Type**</span> : Modelling of behavior based on a <span style="color:MediumAquamarine">**Device Type**</span>.

	<span style="color:LightSeaGreen">**Device**</span> : Instance of a <span style="color:MediumAquamarine">**Device Type**</span>.

	<span style="color:MediumAquamarine">**Device Type**</span> : Modelling of hardware or logical device with WinCC OA datapoints.

A DU corresponds to an SMI <span style="color:Salmon">**Physical Object**</span> and they are always a tree 'leaf' (i.e. DUs have no children) therefore do not implement logic behaviour.

## **FSM in JCOP Framework**

### **Configuration**

SMI like WinCC OA allows the definition of object types and the derivation of objects from the type. In order to create a hierarchy it is necessary to:

* Create any <span style="color:MediumAquamarine">**Device Types**</span>, the types from which <span style="color:SeaGreen">**Device Units**</span> will inherit.
* Create any <span style="color:MediumVioletRed">**Object Types**</span>, the types from which any <span style="color:LightCoral">**Abstract Objects**</span> will inherit.
* Create <span style="color:DodgerBlue">**Control Units**</span> , instantiate the <span style="color:LightSeaGreen">**Devices**</span> and(or) <span style="color:LightCoral">**Abstract Objects**</span> and include in a particular domain.

The FSM is fully integrated with the JCOP Framework and deal in terms of the hardware declared in the **"Hardware View"** of the **"Device Editor Navigator" (DEN)**.

!!! tip "At the lowest level, commands arrive at the DUs and are passed to the hardware (i.e. the items declared either in the DEN 'Hardware View' or the 'Logical View')."

!!! info "All child nodes must always communicate with theri parent nodes using a commonly understood vocabulary if not user must configure into a common form."

!!! danger "Not all children of a given parent and/or sibling CUs CU will be identical. Each must nevertheless use an agreed interface (i.e. use a recognised vocabulary) when communicating with the parent."
	
### **Partitions**

The owner or the operator releases the ownership of the entire FSM or a part of the FSM, so that another operator can work with it.

!!! warning "Only the owner can exclude a component from the hierarchy."
	
Each CU is capable of partitioning 'out' or 'in' of its children. Excluding a child from the hierarchy implies that it’s state is not taken into account by the parent in further processes by sending commands. 

There are 4 defined partitioning modes.

![Placeholder](../assets/images/devguide/FSM/partitions.PNG){ loading=lazy : style="width:690px" } 

--8<-- "docs/includes/abbreviations.md"
