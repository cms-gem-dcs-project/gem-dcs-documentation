# **Configuration Database Tool of JCOP Framework** 

## **Setting Up the Database with ConfigDB**

1.	Hover over the **"JCOP Framework"** and click on **"ConfigurationDB Tool"** option.
	
	![Placeholder](../assets/images/devguide/JCOPfw/JCOP_configDB.PNG){ loading=lazy : style="width:426px" } 

2.	Click on the dropdown menu next to the dialog box **(A)** in the **"CDB setup and status"** section.
	
	![Placeholder](../assets/images/devguide/JCOPfw/configDB_setup.PNG){ loading=lazy : style="width:555px" } 

3.	General setup section,
 
	- [x] In the next popup panel click on the **"Create"** button **(B)** of **"Setup Name"**.
	- [x] Include the database name in the new dialog box that will appear and confirm.
	
		![Placeholder](../assets/images/devguide/JCOPfw/configDB_new.PNG){ loading=lazy : style="width:362px" } 

	- [x] If required make the new database the default by checking the radio box.

4.	Database setup section,

	- [x] Select **"Create"** option from the dropdown menu **(C)** of the **"DB Connection"**.
	- [x] Complete necessary information in the new dialog box that will appear and confirm. 
		
		![Placeholder](../assets/images/devguide/JCOPfw/configDB_credentials.PNG){ loading=lazy : style="width:319px" } 

	- [x] Click on **"Try to Connect"** and wait until the connection is set and then click **"Create"**. If the connection was successful the below dialog box will appear.
		
		![Placeholder](../assets/images/devguide/JCOPfw/configDB_success.PNG){ loading=lazy : style="width:370px" } 

## **Checking Database Status**

!!! note "If the creation of the database is not defined then the DB status will give ‘N/A’ and turn into blue."
	![Placeholder](../assets/images/devguide/JCOPfw/configDB_blue.PNG){ loading=lazy : style="width:379px" } 

!!! check "If the creation of the database is made properly then the DB status will give ‘OK’ and turn into green."
	![Placeholder](../assets/images/devguide/JCOPfw/configDB_green.PNG){ loading=lazy : style="width:380px" } 

!!! failure "If the creation of the database is erroneous then the DB status will give ‘ERROR’ and turn into red." 
	![Placeholder](../assets/images/devguide/JCOPfw/configDB_red.PNG){ loading=lazy : style="width:378px" } 

Before setting up the DB configuration tool the user must have installed Oracle as an environment in the machine that is being used to run the project.

## **Setting Up ORACLE Environment**

1.	If the machine does not have Visual Studio installed then install **'Visual Studio x64 Redistributable 2010'**. 

	!!! info "Several Redistributable application setups are available in the dfs storage drive under <span style="color:blue">Applications / Microsoft / Redistributable</span>."

2.	Download the appropriate **'Instant Client'** packages for the platform the machine.

	!!! tip "All installations require the **'Basic'** package and installation instructions are provided in the directory."

	!!! info "Required versions of Oracle are available in the dfs storage drive under <span style="color:blue">Applications / Oracle / Group / InstantClients</span>." 

3.	Unzip the package into a single directory (i.e. instantclient_12_1).

4.	Add the library loading path in the environment to the directory of the variable. 

	!!! warning "Ensure only one Install Client referenced in the path variable, or at least that the concerned version is the first to appear."

	!!! help "If the environmental variable has not been created previously, users must create a new environment variable to have **TNS_ADMIN** as the variable name and <span style="color:blue">\\cern.ch\dfs\Applications\Oracle\ADMIN</span> as the variable value."

--8<-- "docs/includes/abbreviations.md"
