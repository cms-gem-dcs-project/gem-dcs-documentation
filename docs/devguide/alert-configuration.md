# **Alert Configuration** 

If alarms were designed with regard to the values of properties associated with the DCS, alerts would be activated after surpassing a predefined threshold such that the operator will be notified via emails (phones etc.) in addition to the alarm screen.

??? warning "Notification destination depends on the "Alert Class" number. Propargation of alerts based upon a warning or an error could reach the local DCS destination alone or both local and central DCS."
	**Defined limits for alerts to propagate to the Control Room of central DCS :**
	
	* `_fwWarningAck` **>=** 50
	* `_fwErrorAck` **>=** 70
	* `_fwFatalAck` **>=** 90

Configuration of alerts is done employing the **'Alert System'** panel accessible via the **'CMS Utilities'** tab. Adding new users, defining new notifications, setting DPs for notifications, temporary disabling, etc. can be configured.

1.	Hover over the **'CMS Utitlities'** tab and click on the option **'Alert System'**.

2.	Start the **'CMSfwAlertSystem'** script in the background via WinCC OA Console in order to work with the **'Alert System'** panel without interruptions.

## **Configuring New Users**

### **Adding New Users**

1.	Move to the section **'USERS/ GROUPS / REDIRECTS'** of the **'Alert System'** panel.

2.	Type a user name in the field **'User Name'**, click on the **"ADD USER"** button and click the **"OK"** option on the below dialog box to configure user information manually.

3.	Fill in the user name, email and mobile phone information in the fields of the **'Add User'** popup panel and click on the **"CREATE"** button.

4.	If the process of adding a new user was successful, user information will appear in the tabular area of the section.

	!!! info "User can be selected by a single click on the name in the table and all notifications assigned to that particular user will then appear in 'NOTIFICATIONS' section."

### **Adding Multiple Users**

1.	Move to the section **'USERS/ GROUPS / REDIRECTS'** of the **'Alert System'** panel.

2.	Click on the **"MANAGE MULTIPLE USERS / GROUPS"** button and wait for the new panel to open.
	
	!!! info "Adding multiple users is done by selecting a notification and adding users to recieve that notification."

3.	Select the notification employing the downward arrow in the **'Notification'** section.

4.	Observe the list **'Available Users'**, and add single, multiple or all users by using **">"** or **">>"** buttons. Added users will appear in the **'Selected users'** list.

5.	Click on the **"APPLY"** button and close the panel.

## **Configuring Notifications**

### **Creating New Notifications**

1.	Click on the name of the user being configured from the tabular area of the **'USERS/ GROUPS / REDIRECTS'** section.

2.	Move to the **'NOTIFICATIONS'** section and fill in the fields **'Notif. Name'** and **'Notif, Description'**.

3.	Click on the **"CREATE NOTIFICATION"** button.

4.	If the process of adding new notifications to was successful, added information will appear in the tabular area of the section after assigned the notification to a user and if that particular user was selected.

### **Adding Existing Notifications to a User**

1.	Click on the name of the user being configured from the tabular area of the **'USERS/ GROUPS / REDIRECTS'** section.

2.	Move to the **'NOTIFICATIONS'** section and click on the dropdown arrow in the blank field.

3.	Select the predefined notification required for the selected user and click on the **'ADD NOTIFICATION'** button.

4.	Fill in the user name, notification name, notification type and priority information in the fields of the **'Edit User Notifications'** popup panel and click on the **"APPLY"** button.
	
5.	If the process of adding a notification to the user was successful, added information will appear in the tabular area of the section.

### **Deleting Notifications from a User**

1.	Click on the name of the user being configured from the tabular area of the **'USERS/ GROUPS / REDIRECTS'** section.

2.	Move to the **'NOTIFICATIONS'** section and click on the notification that requires deleting.

3.	Click on the **"DELETE NOTIFICATION"** button and confirm.

4. If the process of deleting a notification assigned to the user was successful, notification information will disappear from the tabular area of the section.

## **Linking Notification Alerts**

1.	Click on the notification being configured from the tabular area of the **'NOTIFICATIONS'** section.

2.	Click the **"PARA Access"** button next to add alert button options.

3.	Select the DP to link with, from the **'Datapoint Selector'** panel and click on the **"ADD ALERT"**.

4. 	If linking multiple alerts is required, click on the **ADD MULTIPLE ALERTS"** button.

5.	In the **'Alert List'** panel that opens, move to the section **'SELECT DPs TO ADD'**, click on the **"PARA Access"** button and select DPs.
	
	!!! info "It is possible to create wild cards by simply selecting one DP and including ?? replacing hardware number associated with the selected DP."
	
6.	If the process of adding an alert list was successful, the list will appear in the tabular area of the **'ALERT LIST'** section after clicking on the **"VIEW ALERT LIST BUTTON"**.

7.	After confirming the list, click on the **"ADD ALERTS"** button.

8.	If the process of adding alerts was successful, the added alerts will appear in the tabular area of the **'INFORMATION'** section of the **'Alert List'** panel and the **'NOTIFICATION ALERTS'** section of the **'Alert System'** panel.

	!!! warning "Alerts will always be added to notification, not users. If alerts are required to be added to users, the actions must be taken accordingly and specifically for that purpose."

## **Saving and Loading Configurations to the Database**

1.	Observe the **"NOT_OK"** status of the **'CONFIGURATION DB CONSISTENCY'** appeared in red. In order to remedy this situation configuration must be saved to the database.

2.	Click on the **"SAVE TO CONFIG DB"** button on the **'Alert System'** panel.

3.	Enter the configuration name in the **'Configuration Name'** dialog box and click **"OK"**.

4.	The progress of saving the configuration will appear in the **'CDB Progress'** popup panel.

5.	If saving of the configuration was successful, the **'CONFIGURATION DB CONSISTENCY'** status will shift to **"OK"** that appears in green.

6.	User can download saved configurations by clicking on the **"LOAD FROM CONFIG DB"** and providing the configuration name of the configuration that needs loading.