# **JCOP Framework for WinCC OA**

!!! danger "JCOP framework = Joint Controls Project framework"

JCOP framework is employed at CERN with the intention of reducing the manpower required with experiment control systems. In order to work with GEM DCS the user must have the JCOP framework installed. 

!!! info "There are 2 types of frameworks available for GEM DCS :"
	1.	**CERN JCOP framework**

	2.	**CMS specific JCOP framework** 

CMS specific JCOP framework is a modification of CERN JCOP framework and consistent with CMS developed support components in addition to being upgraded after extensive debugging and troubleshooting procedures.

!!! warning "CMS specific JCOP framework is preferred over the CERN JCOP framework."

## **Framework Guidelines and Conventions**

All projects shall be developed as distributed projects and language of new projects should be defined to be US English to use WinCC provided panels.

!!! danger "JCOP framework should be <span style="color:red">installed into projects not machines</span> and if successfully installed then more tabs will appear in the menu bar with dropdown menus of options associated with the tabs."

!!! tip "In order to reduce the development effort, improve the re-usability and the maintainability of the application the following are supported and recommended."
	* Selective installation via an installation tool from the framework distribution kit. 
	* Instantiate generic panels with the possibility of inheritance in subsequent modifications via referencing. 
	* Gather functions into libraries if scripts in panels execute identical or similar functions. 

Descriptive text of buttons should always start with a capital letter followed by lower case letters with the exception of the **"OK"** button.

!!! faq "Standard button labels and definitions"
	- [x] **Cancel** : I have changed my mind and therefore I want to return without implementing any change.
	- [x] **Apply** : Apply the changes but don’t close the window yet. All modifications to this point can no-longer be cancelled.
	- [x] **Ok** : Apply anything that hasn’t already been applied and then close.
	- [x] **Close** : Close this window without making further changes. Modifications that have already been applied still stand.

!!! warning "If actions associated with a button is not allowed at a given time then the button should be disabled. If case the button is used to select a pull down menu of options then the button should remain active with disabled menu items."

--8<-- "docs/includes/abbreviations.md"
