# **Remotely Accessing the Virtual Environment**

### **Remote Access to CERN for Windows External Connections**

1.	Visit https://remotedesktop.web.cern.ch/remotedesktop/ via the link.

2.	Go to the **"Remote desktop your desktop"** section.
	
	![Placeholder](../assets/images/devguide/WinCCOA/remote_desktop.png){ loading=lazy : style="width:506px" } 

3.	Select **"Generate a .rdp file to connect to a PC from outside CERN"** from the available options.
	
	![Placeholder](../assets/images/devguide/WinCCOA/remote_desktop_selection.PNG){ loading=lazy : style="width:525px" } 

4.	Enter the machine name (instancename.cern.ch) and click **"OK"** to proceed.

	![Placeholder](../assets/images/devguide/WinCCOA/remote_machine.png){ loading=lazy : style="width:379px" } 

5.	The file will be auto generated and downloaded to the user machine. Select and run the auto generated .rdp file.

	!!! info "This auto generated .rdp file is the remote desktop application. If the location is not reachable for the downloaded file search for mstsc.exe and open the resultant file."

6.	Give the user name in the form CERN\username and connect to finish creating the remote desktop.

### **Remote Access to CERN for Linux External Connections**

	The content has not been updated yet.
	
--8<-- "docs/includes/abbreviations.md"
