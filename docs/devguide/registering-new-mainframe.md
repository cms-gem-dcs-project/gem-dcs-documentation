# **Registering a New Mainframe**

There are 4 mainframes associated with the GEM DCS and each consists of 9 HV boards. After setting up a mainframe, the said mainframe must be registered at CERN.

1.	Visit https://network.cern.ch and select the **"Register Device"** tab.

2.	Fill the mandatory parts in **"Device Information"** and **"Network Interface Card(s)"** sections.
	
	!!! info "Device Information and Network Interface Card(s)"
		* Desired Device Name : ge11hvsetup 
		* Usual Location : 0904 R-0U18 
		* Manufacturer : CAEN
		* Model / Type : SY4527
		* Operating System : OTHER
		* Responsible for the Device : RESSEGOTTI MARTINA
		* Card Type : ETHERNET
		
	!!! tip "Finding the Hardware Address"
		* Connect the mainframe directly to a pc or laptop using a network cable. 
		* Open a web browser and type the default IP address of the mainframe (Refer the device catalogue).
		* Login to the device by entering the credentials 
		* Go to 'Settings' menu and select 'Network Settings'. 
		* Hardware address is under the 'MAC address' 

3.	Submit the request and the confirmation will be received within one or two days. Leave the mainframe on and ensure that the mainframe is connected to the CERN network.

4. If logging into the mainframe remotely is required please follow ['Remote Connection to the Mainframe'](../expertguide/login-mainframe-remotely.md) section.

--8<-- "docs/includes/abbreviations.md"

