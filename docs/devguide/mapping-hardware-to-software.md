# **Mapping Hardware Modules to Software**

DP and DPE are not automatically generated whenever the JCOP framework is adapted for projects. Users must map the associated hardware modules in the system such that they could be handled via software.

??? info "Device Editor Navigator Tool for Mapping"
	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN.PNG){ loading=lazy : style="width:300px" , align=left} 
	
	The mapping process is done employing the **"Device Editor and Navigator"** option available in the **"JCOP Framework"** tab in the menu bar after installing the framework components for a project.

	There are 2 possible modes of the device manager are the **"Operation Mode"** and the **"Configuration Mode"**. 

	In order to proceed with hardware mapping the **"Configuration Mode"** must be utilized.

## **Mapping CAEN Hardware Modules**

1.	Shifting into the **"Configuration Mode" (A)**, select the CAEN directory in the **"Hardware" (B)** tab and click on the **"CAEN" (C)** directory option. 
	
	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_CAEN_config.png){ loading=lazy : style="width:300px" } 

2.	Right click on the selected directory and click on the option **"Add"** from the dropdown menu.

3.	Select on the tab of preference **"Single Device"** or **"Multiple Devices"** from the resulting panel.

4.	For the **"Single Devices"** tab select the type and model of hardware, a device name, select OCPUA as the communication server to complete mandatory requirements.
	
	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_add_single.PNG){ loading=lazy : style="width:600px" } 

5.	For the **"Multiple Devices"** tab select type and model, provide a device name with a sequence range considering the number of devices, select OCPUA as the communication server to complete mandatory requirements.

	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_add_multiple.PNG){ loading=lazy : style="width:600px" } 

6.	Click on **"Apply"** and confirm the device(s) requested in the popup panel.

	!!! tip ""
		![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_add_success.PNG){ loading=lazy : style="width:342px" , align=left} 
		
		**If the device(s) were created and added successfully,**
		
		- [x] A confirming dialog box will appear.
		- [x] Created device(s) will appear as an extension of **"CAEN"** directory.
	
## **Mapping HV Power Supply Modules**

### **Main Frame Mapping**

1. Follow the steps 1 through 6 of **"Mapping CAEN Hardware Modules"** applying following modifications.

	!!! help "Main Frame Modifications" 
		- [x] Ensure to set the Main Frame type SY1527, model SY4527, name of preference, select the default address OCPUA.

		- [x] In case of the **"Multiple Devices"** provide the range considering the number of HV Main Frames of the detector and select an appropriate number of digits.      
	
	!!! info "Type SY1527 and model SY4527 Main Frame devices are utilized in the detector hence the application of the same type and model is recommended in device adding."

	!!! tip "The name of the hardware need not be the serial name or number as the OCPUA communication server processes the mapping through IP addresses and slot numbers."

2. If the Main Frame(s) were successfully added then a single click on an added Main Frame will lead to a structure of the Main Frame.

	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_MF_structure.PNG){ loading=lazy : style="width:400px" }

### **HV Board and Channel Mapping**
 
1.	Right click on a Main Frame that requires HV boards and select the **"Add"** option from the dropdown menu or click on the **"Add Boards"** option in the Main Frame structure.

2.	Follow steps 3 through 6 of **"Mapping CAEN Hardware Modules"** with following modifications.     

	!!! help "HV Board Modifications"
		- [x] Ensure to set the board type CAEN SY1527 Board A1515, model A1515TG, name of preference, select the default address OCPUA.

		- [x] In case of the **"Multiple Devices"** provide the range considering the number of HV boards of the detector and select an appropriate number of digits.

		- [x] Click on the **"Also Create Children"** checkbox in the **"Children"** section.

	!!! info "Type A1515 and model A1515TG HV Boards are utilized in the detector hence the application of the same type and model is recommended in device adding."
		
	!!! tip "Adding channels can be done manually the same way as the other devices by right clicking on each board selecting the **"Add"** option."

3. If the HV Boards were successfully added to the Main Frame(s) then a single click on a relevant Main Frame will lead to a structure of the Main Frame with the added HV Board.

	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_MF_HV_structure.PNG){ loading=lazy : style="width:400px" }

## **Mapping LV Power Supply Modules**

Due to the complexity of the hardware device connection in case of LV power supply the framework mapping for LV is not as straightforward as the mapping of HV power supply to perform manually.

??? cite "After creating the LV Main Frame and necessary number of Branch Controllers the user must create LV Boards that are connected to Easy Crates along with 48V power supplies which if tried will result on the following popup panel."
	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_branch_control_instructions.PNG){ loading=lazy : style="width:500px" }

In order to configure connections from Branch Controllers to LV Boards follow the method below.

### **Installing EASY Rack Builder Tool**

1.	Visit https://www.caen.it/download/?filter=EASY_XML and install the application software **"EASY Rack Builder Software"**.

2.	Install relevant configuration files for required LV Boards and 48V MAO power supply for the LV system according to following information.
		
	!!! info "LV Board and MAO Configuration Filesshould be set to the LV board type A1527 Board and the 48V MAO power supply A3486S."
 
### **Utilizing EASY Rack Builder tool**
 
1.	Open the .jar file in the installed **"EASY Rack Builder"** directory.

	![Placeholder](../assets/images/devguide/HardwareSoftware/EASY_crates.PNG){ loading=lazy : style="width:500px" }

2.	Select the required LV Board and MAO types from the **"CAEN EASY Boards"** list on the left side of the panel.

3.	Drag and drop the LV Board that appears at the bottom left corner in the **"Selected Board"** region, to the correct slot in the relevant Crate.

4.	Save the configuration or the modification using the appropriate option from **"EASY Rack Configuration"** menu in .xml format to a destination directory of preference.
	
	!!! help "The 48V MAO should be installed and added into separate Crates aligned with the Crates consisting of LV Boards for the system to distribute instructions and perform properly."

	!!! info "It is recommended to save the configurations into a separate directory rather than existing directories by default in the **"CAEN EASY Rack Builder"** directory."
		
	!!! warning "If the required LV Boards and 48V MAO power supplies are not installed into the **"Boards"** directory, the list of **"CAEN EASY Boards"** will not appear on the left side."
	
	!!! failure "EASY Rack Builder tool might not work if the user machine has no Java application installed therefore if there is no Java application installed before proceeding."

5. If the LV Boards and MAO were successfully added to Easy Crates then a single click on a configured design will lead to a structure of Easy Crates with added devices.

	![Placeholder](../assets/images/devguide/HardwareSoftware/EASY_crate_config.PNG){ loading=lazy : style="width:500px" }

###	**LV Power Supply Mapping**

1.	Follow steps 1 through 6 of **"Mapping CAEN Hardware Modules"** to create the LV Main Frame and Branch Controllers following the modifications.

	!!! help "Branch Controller Modifications"
		- [x] Ensure to set the board type CAEN SY1527 Board A1676, model A1676A, name of preference, select the default address OCPUA.

		- [x] In case of the **"Multiple Devices"** provide the range considering the number of HV boards of the detector and select an appropriate number of digits

2.	Click on the created Branch Controller and select **"Advanced Options"** from the informative panel. 

3.	Click on the folder icon **(A)** and select the saved .xml LV Crate configuration file and parse the file to the Branch Controller by clicking on **"Parse File"** button **(B)**.

	![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_branch_control_adv.PNG){ loading=lazy : style="width:532px" }

	!!! tip "" 
		![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_LV_parse.PNG){ loading=lazy : style="width:342px" , align=left}

		**If the file was parsed successfully,**
		
		- [x] A confirming dialog box will appear. 
		- [x] Created device(s) will appear in a tabular format in the panel.
		
4.	Click on **"Create Devices in Table" (C)** option to map the LV power supply. 

	!!! tip "" 
		![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_LV_devices_created.PNG){ loading=lazy : style="width:342px" , align=left}
		
		**If the device(s) were created and added successfully,** 
		
		- [x] A confirming dialog box will appear.
		- [x] Created device(s) will appear as an extension of **"CAEN"** directory.

5.	Click on **"Set Address" (D)** option to connect the OCPUA communication server with created devices. 
	
	!!! warning "Do not close the **"Advanced Settings"** panel before setting the communication server with the created device(s)."

	!!! info "Type A1676 and model A1676A Branch Controllers are utilized in the detector hence the application of the same type and model is recommended in device adding."

	!!! tip ""
		![Placeholder](../assets/images/devguide/HardwareSoftware/DEN_LV_address.PNG){ loading=lazy : style="width:342px" , align=left}
		
		**If the address(s) for device(s) were set successfully,** 
		
		- [x] A confirming dialog box will appear.
		- [x] Created device(s) will appear as an extension of **"CAEN"** directory.

--8<-- "docs/includes/abbreviations.md"
