# **Getting Started with JCOP Framework** 

## **Installing JCOP Framework Components**

1.	Hover over the tab **"JCOP framework"** then click on the **"Installation"** option in the dropdown menu and wait for the popup panel to appear.

	![Placeholder](../assets/images/devguide/JCOPfw/JCOP_installation.PNG){ loading=lazy : style="width:426px" } 

2.	Click on the folder icon **(A)** and include the path to the framework directory and wait until the files are done being loaded.
	
	![Placeholder](../assets/images/devguide/JCOPfw/installing_components.png){ loading=lazy : style="width:535px" } 

	!!! info "Installing necessary components from **"jcop-framework-8.4.1"** is recommended."

3.	Select the names of necessary tools checking with appropriate versions and click on the relevant cell **(B)** in the **"Install"** column to install components.

	!!! info "Similarly install CMS developed tools from **"FrameworkInstallation316_841"** directory if required but not available in **"jcop-framework-8.4.1"** directory."
	
	!!! tip "Follow the first 4 steps of **"Setting Up GEM DCS Components"** section and refer to the requirements mentioned in XML documents to install correct components."
	
4.	Click on the **"Install"** button **(C)** and confirm the download destination.

	!!! help "User can give the project name or a distinguishable name for the destination file replacing the date at the end of it."

	!!! info "In cases of dependency inquiries always select **"Install all"** option if not sure on the course of action or dependencies are explicitly not required."
		
		![Placeholder](../assets/images/devguide/JCOPfw/fwpopup_dependency.PNG){ loading=lazy : style="width:515px" } 

5.	Restart the project and check if the selected tools were installed. 

	![Placeholder](../assets/images/devguide/JCOPfw/fwpopup_restart.PNG){ loading=lazy : style="width:365px" } 

## **Identifying Component Status**

### **Component Status**

!!! check "Successfully Installed Components"
	If successfully installed, components appear normally in **'Installed Compnents'** section. Check **(I)** and **(II)** of the image.
	
!!! warning "'Corrupt' and 'Errornuous' Components"
	![Placeholder](../assets/images/devguide/JCOPfw/fwinstall_fails.PNG){ loading=lazy : style="width:280px" align=left} 
		
	* If the component is highlighted in **yellow**, the **component is corrupt**. The solution is to **Re-Install** the component.

	* If the component highlighted in **red**, the **component is erroneous**. The solution is to **Delete and Install** the component.
	
### **Actions Regarding Component Status**

1.	All installed components will be in the **'Installed Components'** section. 
	
	![Placeholder](../assets/images/devguide/JCOPfw/fwinstall_status.PNG){ loading=lazy : style="width:600px" } 

2.	Click on the relevant cell(s) in the 'Delete' column **(D)** to get an erroneous or an unwanted file deleted. Click the **"Delete"** button **(E)** to delete.
	
3.	Click on the relevant cell(s) where the component is already installed in the 'Install' column to reinstall. After the cell shows **"Re-Install"** in the selected cell **(F)** click on **"Install"** button. 

--8<-- "docs/includes/abbreviations.md"
