# **Creating a Virtual Environment**

## **Creating a Virtual Environment in Windows**
 
1.	Access http://openstack.cern.ch using personal CERN credentials.

2.	Click on the **"Compute" (II)** section from the **"Project" (I)** category from the left menu.
	
	![Placeholder](../assets/images/devguide/WinCCOA/VM.PNG){ loading=lazy : style="width:371px" } 

3.	Click on **"Images" (III)** and select **"Create Volume" (A)** from the dropdown menu for the required Windows virtual image.
	
	![Placeholder](../assets/images/devguide/WinCCOA/VM_images.PNG){ loading=lazy : style="width:792px" } 

4.	After the volume reaches an active state click on **"Volume" (IV)** from the **"Project"** category from the left menu.

5.	Click on the volume created earlier and select **"Launch Instance" (B)** from the dropdown menu. 
	
	![Placeholder](../assets/images/devguide/WinCCOA/VM_volume.PNG){ loading=lazy : style="width:793px" } 

6.	Complete mandatory information and launch the virtual machine. 
	
	![Placeholder](../assets/images/devguide/WinCCOA/VM_launch.PNG){ loading=lazy : style="width:535px" } 

## **Creating a Virtual Environment in Linux**

	The content has not been updated yet.
	
--8<-- "docs/includes/abbreviations.md"
