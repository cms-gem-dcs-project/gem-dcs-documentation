# **Getting Started with WinCC OA**

It is possible for users to run WinCC OA using the application tool. There are many tools made available in WinCC OA for administration, configuration, editing, archiving etc. In general WinCC OA is a tool for building SCADA applications.

!!! check "WinCC OA has the capability of device description, access control, alarm handling and display, filtering, logging, summarizing, trending, archiving and UI building etc."

!!! warning "WinCC OA does not have specific tools for abstract behaviour modelling (Finite State Machines) or automation and error recovery (Expert Systems)."  

## **WinCC OA Project Administrator (PA)**

Clicking on the WinCC OA application tool will lead the user to the **'Project Administrator'** panel. The panel displays current projects installed in the system, their versions and the process monitoring status. 

??? cite "Project Administrator Panel"
	![Placeholder](../assets/images/devguide/WinCCOA/PA.PNG){ loading=lazy : style="width:581px" } 

??? info "Header Section" 
	![Placeholder](../assets/images/devguide/WinCCOA/PA_info.PNG){ loading=lazy : style="width:582px" } 

!!! tip "Multiple projects can be administered in 1 machine but only 1 project can be started at a given instance. Projects are runnable and used alongside multiple supporting interfaces, scripts, etc." 

## **WinCC OA Systems**

There are 4 main types of systems associated with WinCC OA.

### **Creating a WinCC OA Project**

The **'Project Administrator'** panel is used to create projects. The following steps are to create a distributed project using the PA panel.

!!! danger "In WinCC 'Distributed Systems', each system name and system number must be unique. User must ensure that this factor is satified whilst creating a project for such systems."

1.	Click on the **"New Project"** icon in the header section of the PA.
	
	![Placeholder](../assets/images/devguide/WinCCOA/project_create.PNG){ loading=lazy : style="width:609px" } 

2.	Select the required type of the project in the **'Project Type Settings'** section of the **'New Project'** popup window and click the **"Next >"** button.
	
	![Placeholder](../assets/images/devguide/WinCCOA/project_new.PNG){ loading=lazy : style="width:572px" } 

	!!! tip "In the case of the example, selected type is "Distributed Project" and once the user clicked on the "Next >" button, the 'Project Type Settings' section will have a tick indicating the completion"

3.	Provide a name for the project being created, select a language and provide the path to the exxpected destination of the project in the **'General Settings'** section and click the **"Next >"** button.
	
	![Placeholder](../assets/images/devguide/WinCCOA/project_settings.PNG){ loading=lazy : style="width:572px" } 

4.	Fill in the information on system name and number in the **'Distributed Settings'** section and click the **"Next >"** button.
	
	![Placeholder](../assets/images/devguide/WinCCOA/project_specific.PNG){ loading=lazy : style="width:572px" } 

5. 	In the final stage, a summary of the settings could be seen and double check to confirm the accuracy. Click on **"OK"** to create the project.
	
	![Placeholder](../assets/images/devguide/WinCCOA/project_finalize.PNG){ loading=lazy : style="width:572px" } 

	!!! tip "After a moment the newly created project will appear in the **'Project Administrator'** panel under the list of projects in the system"
	
## **WinCC OA Console**
 
The WinCC OA Console provides the user with project manager information and is used for starting, monitoring and stopping project managers.

Selecting a project and then clicking on the **"Start Project"** icon **(A)** leads to the console automatically. If not click on the **"Open Project Console and Start PMON"** icon **(B)**. 

![Placeholder](../assets/images/devguide/WinCCOA/get_console.PNG){ loading=lazy : style="width:606px" } 

Process Monitor or the PMON handles starting, configuring, stopping and monitoring project managers.

??? cite "Console"
	![Placeholder](../assets/images/devguide/WinCCOA/console.PNG){ loading=lazy : style="width:393px" } 

??? tip "Color Code"
	In the monitoring panel each color has a meaning based upon the status of the respective project manager type.

	![Placeholder](../assets/images/devguide/WinCCOA/console_color_code.PNG){ loading=lazy : style="width:581px" } 

There are 3 sections in the console of WinCC OA.

??? info "Header section : General tools" 
	![Placeholder](../assets/images/devguide/WinCCOA/console_header.PNG){ loading=lazy : style="width:491px" } 

??? info "Project section : Project specific tools" 
	![Placeholder](../assets/images/devguide/WinCCOA/console_project.PNG){ loading=lazy : style="width:488px" } 

??? info "Manager section : Project monitoring tools" 
	![Placeholder](../assets/images/devguide/WinCCOA/console_manager.PNG){ loading=lazy : style="width:543px" } 

## **WinCC OA Log Viewer**

The Log Viewer is the output unit for system information of processing managers and therefore is recommended for the system experts and administrators.
 
The Log Viewer window automatically pops up when a project is started. If it is not, users can click on the **"Log Viewer"** icon to open the Log Viewer window. 

??? cite "Log Viewer Window"
	![Placeholder](../assets/images/devguide/WinCCOA/logviewer.PNG){ loading=lazy : style="width:600px" } 

There are 2 tabs in the menu bar and tools in each of these menus are included in the tool bar of the Log Viewer window.

??? info "File menu" 
	![Placeholder](../assets/images/devguide/WinCCOA/logviewer_file.PNG){ loading=lazy : style="width:534px" } 

??? info "View menu" 
	![Placeholder](../assets/images/devguide/WinCCOA/logviewer_view.PNG){ loading=lazy : style="width:599px" } 

## **WinCC OA Graphical Editor (GEDI)**

The Graphical Editor commonly referred to as GEDI is used for creating and editing panels. It  provides tools for creating and configuring graphic objects which can be graphical representations of complete systems or just simple operating screens.

??? cite "Graphical Editor Panel"
	![Placeholder](../assets/images/devguide/WinCCOA/GEDI.PNG){ loading=lazy : style="width:1366px" } 

WinCC OA GEDI has multiple sections, menus and toolbars designed for different tasks and different panels to view libraries, current properties, etc. 

??? info "**Menu Bar**"
	The Menu Bar of GEDI consists of the above tabs each having a dropdown menu of all options available for the selected menu.
	
	![Placeholder](../assets/images/devguide/WinCCOA/GEDI_menubar.PNG){ loading=lazy : style="width:728px" }

??? info "**Tool Bars**"
	Tool Bars of GEDI consists of the many icons each having a different purpose in designing grphical objects. These icon present shortcuts for the tools as all of these are options available in dropdown menus of the Menu Bar.
	
	![Placeholder](../assets/images/devguide/WinCCOA/GEDI_toolbars.PNG){ loading=lazy : style="width:824px" }

	**Main Tools (2) :** 
	
	- [x] The Main Tools toolbar displays by default and includes standard functions available in Panel and Edit menus. 
	
	??? info "I.	Basic tools"
		![Placeholder](../assets/images/devguide/WinCCOA/GEDI_main_basic.PNG){ loading=lazy : style="width:491px" } 
		
	??? info "II.	Editing tools"
		![Placeholder](../assets/images/devguide/WinCCOA/GEDI_main_edit.PNG){ loading=lazy : style="width:491px" } 
		
	??? info "III.	Layout tools"
		![Placeholder](../assets/images/devguide/WinCCOA/GEDI_main_layout.PNG){ loading=lazy : style="width:613px" }

	**Basic Objects Toolbar (3) :** 
	
	- [x] Users can create basic graphic objects by employing tools available in the Basic Objects toolbar. 
	
	**Extended Objects Toolbar (4) :** 
	
	- [x] Users can create extended graphics objects employing the tools available in the Extended Objects icon bar. 
	
	**Format Tools (5) :** 
	
	- [x] Users can format objects employing the tools available in the Format Tools icon bar.
	
	**EWOs Tools (6) :** 
	
	- [x] Extended Widget Objects allow the user to embed externally designed objects.

	**View Tools (7) :** 
	
	- [x] Users can set the view of the panel window employing the tools available in the View Tools icon bar.

??? info "Project View"   
	![Placeholder](../assets/images/devguide/WinCCOA/GEDI_project_view.PNG){ loading=lazy : style="width:259px" , align=left}
	
	**"Project View" tab provides information on,** 
	
	* Panels
	* Scripts
	* Libraries
	* Message catalogues 
	* Pictures of current projects
	* Embedded subprojects 
	* WinCC OA installation directory
	
	
??? info "Property Editor" 
	![Placeholder](../assets/images/devguide/WinCCOA/GEDI_property_editor.PNG){ loading=lazy : style="width:267px" , align=left}
	
	The Property Editor is employed to edit object properties of graphic objects created in a panel. It has 2 tabs, 
	
	* Standard 	: General object properties are available (color, size, position etc.)
	* Extended 	: Object specific properties (word wrap, scroll bar modes etc.)
	



