# **Installing WinCC OA**

WinCC OA is the **"Supervisory Control And Data Acquisition" (SCADA)** system utilized as a tool in the process of designing the GEM DCS. 

!!! tip "It is currently owned by Siemens, after claiming the ownership of original commercial product introduced by ETM, Austraia. Before the takeover WinCC OA was known as "PVSS"." 

WinCC OA has been widely adopted across CERN with the possibility of operating on multiple platforms, openness with regard to programming and the scalability that allows large distributed systems.

## **Installing WinCC OA in Windows**

1.	Visit the link https://readthedocs.web.cern.ch/display/ICKB/WinCCOA+Service+Download+3.16 to access WinCC OA service downloadable versions. 

	!!! tip ""
		**Alternate Search: WinCCOA Service Download 3.16 - Industrial Controls - Read The Docs (cern.ch)**

2.	Select the latest recommended WinCC OA version for Windows operating systems.

3.	Login to the website using personal CERN credentials.

4.	Download and install the relevant .exe file.

## **Installing WinCC OA in Linux**

1.	Visit the link https://readthedocs.web.cern.ch/display/ICKB/WinCCOA+Service+Download+3.16 to access WinCC OA service downloadable versions.

	!!! tip "" 
		**Alternate Search : WinCCOA Service Download 3.16 - Industrial Controls - Read The Docs (cern.ch)**

2.	Select the latest recommended WinCC OA version for Linux operating systems.

3.	Login to the website using your CERN credentials.

4.	Download and install the relevant .exe file.

--8<-- "docs/includes/abbreviations.md"
