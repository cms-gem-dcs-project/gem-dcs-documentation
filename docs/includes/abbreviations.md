*[LHC]:Large hadron Collider
*[CMS]:Compact Muon Solenoid
*[GEM]:Gas Electron Multiplier
*[DCS]:Detector Control System
*[DAQ]:Data Acquisition
*[HV]:High Voltage
*[LV]:Low Voltage
*[FSM]:Finite State Machine
*[DP]:Data Point
*[DPT]:Data Point Type
*[DPE]:Data Point Element
*[DU]:Device Unit
*[CU]:Control Unit
*[LU]:Logical Unit
*[DNS]:DIM Name Server